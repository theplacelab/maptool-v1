// gatsby-node.js
// https://www.docz.site/docs/customizing-webpack-config
const path = require('path');
exports.onCreateWebpackConfig = args => {
  args.actions.setWebpackConfig({
    resolve: {
      // ⚠ Note the '..' in the path because the docz gatsby project lives in the `.docz` directory
      modules: [
        path.resolve(__dirname, '../packages/client/src'),
        'node_modules'
      ],
      alias: {
        '@': path.resolve(__dirname, '../packages/client/src/components/')
      }
    }
  });
};
