/* eslint-disable no-undef */
// Link.react.test.js
import React from 'react';
import FormInput, { FIELDTYPE } from './index.js';
import renderer from 'react-test-renderer';
const onChange = (e) => {
  console.log(e);
};

test('Description of the test', () => {
  window.URL.createObjectURL = () => console.log('hi');
  const component = renderer.create(
    <FormInput
      hasOptionToggles={true}
      label={FIELDTYPE.TEXT}
      id={FIELDTYPE.TEXT}
      type={FIELDTYPE.TEXT}
      onChange={onChange}
      isPublished={true}
      isLocked={false}
      isDisabled={false}
      options={{ data: Math.random() }}
      value="Test"
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
