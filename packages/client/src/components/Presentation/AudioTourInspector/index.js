import PropTypes from 'prop-types';
import React, { Component } from 'react';
import FormInput from 'components/FormInput';
import styles from './AudioTourInspector.module.css';
import IconButton from '../../IconButton';
import { coerceJSON, missingValue, makeAssetURL, getByID } from 'utility';
import memoizeOne from 'memoize-one';
import Collapsible from 'react-collapsible';
import ReactMediaPlayer from 'react-player';

class AudioTourInspector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dialog: {
        isVisible: true,
        confirmLabel: '',
        cancelLabel: '',
        message: '',
        onConfirm: '',
        onCancel: ''
      }
    };
    this.m_parseInteractionTree = memoizeOne(
      (media_config, availableOptions, availableMedia) => {
        return this.parseInteractionTree(
          media_config,
          availableOptions,
          availableMedia
        );
      }
    );
    this.m_buildUI = memoizeOne((interactionTree, availableOptions) => {
      return this.buildUI(interactionTree, availableOptions);
    });
    this.m_unusedTriggers = memoizeOne((triggers, config) => {
      return this.unusedTriggers(triggers, config);
    });
  }
  render() {
    if (missingValue([this.props.poi, this.props.mediaOptions])) {
      return null;
    }

    let ui = this.m_buildUI(
      this.m_parseInteractionTree(
        this.props.poi.media_config,
        this.props.mediaOptions.available
      ),
      this.props.mediaOptions.available
    );
    let selectionOptions = [
      ...[{ name: 'Add Gate:', id: null }],
      ...(this.props.mediaOptions.available.triggers !== null
        ? this.props.mediaOptions.available.triggers
        : [])
    ];
    return (
      <div className={styles.container}>
        <div className={styles.title}>{this.props.poi.name}</div>
        {this.props.mediaOptions.available.triggers !== null && (
          <div className={styles.addTrigger}>
            <FormInput
              styles={styles}
              key="triggerAdd"
              type="select"
              metaData={{ type: 'trigger', poiID: this.props.poi.id }}
              selectionOptions={selectionOptions}
              label={null}
              isDisabled={false}
              onChange={this.onChange}
            />
          </div>
        )}
        {ui}
      </div>
    );
  }
  buildConfig = (id, overrides, availableConfigs) => {
    let thisConfig = getByID(id, availableConfigs);
    if (missingValue([id, thisConfig])) {
      return {};
    }
    let defaults_copy = JSON.parse(JSON.stringify(thisConfig.options_template));
    delete thisConfig['options_template'];
    return {
      ...thisConfig,
      options: { ...defaults_copy, ...overrides }
    };
  };
  defaultTracks = (defaultTracks, availableTransitions) => {
    if (!defaultTracks) return;
    let defaultTransition = 0;
    let tracks = {};
    defaultTracks.forEach((availableTrack) => {
      let availableTrack_copy = JSON.parse(JSON.stringify(availableTrack));
      //let thisTrack = interactionTree[poiTriggerID].tracks[availableTrack_copy.id];
      let thisTrack = availableTrack_copy;
      thisTrack.transition = getByID(defaultTransition, availableTransitions);
      thisTrack.media = null;
      tracks = { ...tracks, [availableTrack_copy.id]: thisTrack };
    });
    return { tracks: tracks };
  };

  parseInteractionTree = (media_config, availableOptions) => {
    if (missingValue(media_config)) {
      return null;
    }

    let interactionTree = {};

    media_config.forEach((poitrigger) => {
      interactionTree[poitrigger.id] = {
        ...this.buildConfig(
          poitrigger.trigger_id,
          poitrigger.trigger_options,
          availableOptions.triggers
        ),
        trigger_id: poitrigger.trigger_id,
        ...this.defaultTracks(
          availableOptions.tracks,
          availableOptions.transitions
        )
      };
    });

    media_config.forEach((poitrigger) => {
      if (poitrigger.track_id !== null) {
        interactionTree[poitrigger.id] = {
          ...interactionTree[poitrigger.id],
          tracks: {
            ...interactionTree[poitrigger.id].tracks,
            [poitrigger.track_id]: {
              poi_trigger_config_id: poitrigger.poi_trigger_config_id,
              name: getByID(poitrigger.track_id, availableOptions.tracks).name,
              description: getByID(poitrigger.track_id, availableOptions.tracks)
                .description,
              transition: {
                ...this.buildConfig(
                  poitrigger.transition_id,
                  poitrigger.transition_options,
                  availableOptions.transitions
                ),
                transition_id: poitrigger.transition_id
              },
              media:
                poitrigger.media_id !== null
                  ? getByID(poitrigger.media_id, availableOptions.media)
                  : null
            }
          }
        };
      }
    });
    return interactionTree;
  };

  buildUI = (interactionTree, availableOptions) => {
    if (missingValue(interactionTree)) {
      return null;
    }
    let ui = [];
    Object.keys(interactionTree).forEach((poi_trigger_id) => {
      ui.push(
        <Collapsible
          key={poi_trigger_id}
          open={true}
          transitionCloseTime={150}
          transitionTime={150}
          trigger={
            <div className={styles.triggerHeader}>
              <div>{interactionTree[poi_trigger_id].name}</div>
              <div className={styles.removeAction}>
                <IconButton
                  styles={styles}
                  icon="minus-circle"
                  isBorderless={true}
                  onClick={(e) => {
                    e.stopPropagation();
                    this.props.mediaOptions.onDeleteTrigger(
                      parseInt(poi_trigger_id)
                    );
                  }}
                />
              </div>
            </div>
          }
        >
          <div
            className={styles.triggerBlock}
            key={`trigger_${poi_trigger_id}`}
          >
            {this.buildOptionUI(
              <div className={styles.triggerOptionsHeader}>
                &quot;{interactionTree[poi_trigger_id].name}&quot; Options
              </div>,

              styles.triggerOptions,
              interactionTree[poi_trigger_id].options,
              {
                type: 'trigger',
                poi_trigger_id: poi_trigger_id,
                poiID: this.props.poi.id
              }
            )}
            <div key={`trigger_${poi_trigger_id}`}>
              {Object.keys(interactionTree[poi_trigger_id].tracks).map(
                (trackid) => {
                  return (
                    <div className={styles.trackBlock} key={`track_${trackid}`}>
                      <div className={styles.trackHeader}>
                        <div>
                          {trackid}:{' '}
                          {interactionTree[poi_trigger_id].tracks[trackid].name}
                        </div>
                      </div>
                      {interactionTree[poi_trigger_id].tracks[trackid]
                        .transition !== null && (
                        <div>
                          <div className={styles.transitionSelect}>
                            <FormInput
                              styles={styles}
                              key={`transition_${trackid}`}
                              type="select"
                              selectionOptions={availableOptions.transitions}
                              label={null}
                              metaData={{
                                type: 'transition',
                                poi_trigger_config_id:
                                  interactionTree[poi_trigger_id].tracks[
                                    trackid
                                  ].poi_trigger_config_id,
                                trackID: parseInt(trackid),
                                poi_trigger_id: parseInt(poi_trigger_id),
                                media_id:
                                  interactionTree[poi_trigger_id].tracks[
                                    trackid
                                  ].media !== null
                                    ? interactionTree[poi_trigger_id].tracks[
                                        trackid
                                      ].media.id
                                    : null
                              }}
                              isDisabled={false}
                              value={
                                interactionTree[poi_trigger_id].tracks[trackid]
                                  .transition.id
                              }
                              onChange={this.onChange}
                            />
                          </div>

                          {interactionTree[poi_trigger_id].tracks[trackid]
                            .transition.mediaRequired && (
                            <div className={styles.mediaSelect}>
                              <FormInput
                                styles={styles}
                                key={`media_${trackid}`}
                                type="select"
                                selectionOptions={availableOptions.media}
                                label={null}
                                metaData={{
                                  type: 'media',
                                  poi_trigger_config_id:
                                    interactionTree[poi_trigger_id].tracks[
                                      trackid
                                    ].poi_trigger_config_id,
                                  trackID: parseInt(trackid),
                                  poi_trigger_id: parseInt(poi_trigger_id)
                                }}
                                isDisabled={false}
                                value={
                                  interactionTree[poi_trigger_id].tracks[
                                    trackid
                                  ].media === null
                                    ? ''
                                    : interactionTree[poi_trigger_id].tracks[
                                        trackid
                                      ].media.id
                                }
                                onChange={this.onChange}
                              />
                            </div>
                          )}
                          {interactionTree[poi_trigger_id].tracks[trackid]
                            .transition.options_template !== null &&
                            this.buildOptionUI(
                              <React.Fragment>
                                {interactionTree[poi_trigger_id].tracks[trackid]
                                  .media !== null && (
                                  <ReactMediaPlayer
                                    style={{
                                      margin: '.6rem 0 0 2rem',
                                      height: '1rem',
                                      width: '16rem'
                                    }}
                                    src={makeAssetURL(
                                      interactionTree[poi_trigger_id].tracks[
                                        trackid
                                      ].media.url
                                    )}
                                    controls
                                  />
                                )}
                                <div className={styles.transitionOptionsHeader}>
                                  &quot;
                                  {
                                    interactionTree[poi_trigger_id].tracks[
                                      trackid
                                    ].transition.name
                                  }
                                  &quot; Options
                                </div>
                              </React.Fragment>,
                              styles.transitionOptions,
                              interactionTree[poi_trigger_id].tracks[trackid]
                                .transition.options,
                              {
                                type: 'transition',
                                poi_trigger_id:
                                  interactionTree[poi_trigger_id].tracks[
                                    trackid
                                  ].poi_trigger_id,
                                poi_trigger_config_id:
                                  interactionTree[poi_trigger_id].tracks[
                                    trackid
                                  ].poi_trigger_config_id,
                                poitrigger_id: parseInt(poi_trigger_id),
                                track_id: parseInt(trackid),
                                transition_id:
                                  interactionTree[poi_trigger_id].tracks[
                                    trackid
                                  ].transition.id,
                                transition_options:
                                  interactionTree[poi_trigger_id].tracks[
                                    trackid
                                  ].transition.options,
                                media_id:
                                  interactionTree[poi_trigger_id].tracks[
                                    trackid
                                  ].media === null
                                    ? ''
                                    : interactionTree[poi_trigger_id].tracks[
                                        trackid
                                      ].media.id
                              }
                            )}
                        </div>
                      )}
                    </div>
                  );
                }
              )}
            </div>
          </div>
        </Collapsible>
      );
    });
    return ui;
  };

  buildOptionUI = (trigger, styles, options, metadata) => {
    if (typeof options === 'undefined') {
      return;
    }
    options = coerceJSON(options);
    metadata = coerceJSON(metadata);
    let ui = [];
    Object.keys(options).forEach((field, idx) => {
      ui.push(
        <div key={`${field}_${idx}`}>
          <FormInput
            styles={styles}
            type="text"
            metaData={{ ...metadata, name: field, options: options }}
            label={field}
            value={options[field]}
            onChange={this.onChange}
          />
        </div>
      );
    });

    return missingValue(ui) ? null : (
      <Collapsible
        open={false}
        transitionCloseTime={150}
        transitionTime={150}
        trigger={trigger}
      >
        <div className={styles}>{ui}</div>
      </Collapsible>
    );
  };

  onChange = (e) => {
    if (missingValue(e.currentTarget.dataset['meta'])) {
      return;
    }
    let data = JSON.parse(e.currentTarget.dataset['meta']);
    let value = parseInt(e.currentTarget.value);
    data = { ...data, [`${data.type}ID`]: value };

    switch (data.type) {
      case 'trigger':
        if (typeof data.options !== 'undefined') {
          let payload = {
            id: parseInt(data.poi_trigger_id),
            poi_id: parseInt(data.poiID),

            trigger_options: {
              ...data.options,
              [data.name]: value
            }
          };
          this.props.mediaOptions.onUpdateTrigger(payload);
        } else {
          this.props.mediaOptions.onCreateTrigger({
            poi_id: parseInt(data.poiID),
            trigger_id: parseInt(data.triggerID),
            trigger_options: null
          });
        }

        break;

      case 'transition': {
        let transitionOptions = data.transition_options;
        if (typeof transitionOptions === 'undefined') {
          transitionOptions = null;
        }
        if (typeof data.name !== 'undefined') {
          transitionOptions = {
            ...data.transition_options,
            [data.name]: value
          };
          data.transitionID = data.transition_id;
        }
        if (typeof data.poi_trigger_config_id !== 'undefined') {
          this.props.mediaOptions.onUpdateTriggerConfig({
            id: data.poi_trigger_config_id,
            poitrigger_id: data.poi_trigger_id,
            track_id: data.trackID,
            media_id: data.mediaID,
            transition_options: transitionOptions,
            transition_id: data.transitionID
          });
        } else {
          this.props.mediaOptions.onCreateTriggerConfig({
            id: data.poi_trigger_id,
            poitrigger_id: data.poi_trigger_id,
            track_id: data.trackID,
            media_id: data.mediaID,
            transition_options: transitionOptions,
            transition_id: data.transitionID
          });
        }
        break;
      }

      case 'media':
        if (typeof data.poi_trigger_config_id !== 'undefined') {
          this.props.mediaOptions.onUpdateTriggerConfig({
            id: data.poi_trigger_config_id,
            poitrigger_id: data.poi_trigger_id,
            track_id: data.trackID,
            media_id: data.mediaID,
            transition_id: data.transitionID
          });
        }
        break;

      default:
        console.error("I don't know how to: " + data.type);
    }
  };

  /*
	unusedTriggers = (triggers, config) => {
		let usedIDs = [];
		let unused = [{ name: "Add Gate:", id: null }];
		if (config !== null) {
			config.forEach(item => {
				usedIDs.push(parseInt(item.trigger_id));
			});
		}
		triggers.forEach(trigger => {
			//if (!usedIDs.includes(parseInt(trigger.id))) {
			let thisTrigger = { ...trigger, name: `${"  "}${trigger.name}` };
			unused.push(thisTrigger);
			//}
		});
		return unused;
	};*/
}

AudioTourInspector.propTypes = {
  mediaOptions: PropTypes.shape({
    available: PropTypes.shape({
      triggers: PropTypes.any
    }),
    onCreateTrigger: PropTypes.func,
    onCreateTriggerConfig: PropTypes.func,
    onDeleteTrigger: PropTypes.func,
    onUpdateTrigger: PropTypes.func,
    onUpdateTriggerConfig: PropTypes.func
  }),
  poi: PropTypes.shape({
    id: PropTypes.any,
    media_config: PropTypes.any,
    name: PropTypes.any
  })
};
export default AudioTourInspector;

AudioTourInspector.defaultProps = {};
