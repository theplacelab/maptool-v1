import React, { Component } from 'react';
import PropTypes from 'prop-types';

class BearingArrow extends Component {
  render() {
    if (this.props.showArrow && this.props.bearing) {
      let opacity = 100 / this.props.distanceToDestination;
      opacity = opacity > 1 ? 1 : opacity <= 0 ? 0.1 : opacity;
      return (
        <div
          style={{
            color: 'rgb(255, 112, 8)',
            margin: '0 0 0 1rem',
            fontSize: '1.5rem',
            transform: `rotate(${this.props.bearing * -1}deg)`,
            opacity: opacity
          }}
          className="fas fa-arrow-circle-right"
        />
      );
    } else {
      return (
        <div
          style={{
            color: '#a0ff7a',
            margin: '0 0 0 1rem',
            fontSize: '1.5rem'
          }}
          className="fas fa-dot-circle"
        />
      );
    }
  }
}
export default BearingArrow;

BearingArrow.defaultProps = {
  bearing: 0,
  distanceToDestination: 0
};
BearingArrow.propTypes = {
  showArrow: PropTypes.bool,
  bearing: PropTypes.number,
  distanceToDestination: PropTypes.number
};
