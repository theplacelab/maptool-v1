/*
  Accepts Events:
    audioTourTrigger
    audioTourReset
    audioTourDuck
    audioTourUnDuck

*/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './AudioTour.module.css';
import ReactPlayer from 'react-player';
import { makeAssetURL, missingValue } from 'utility';
import CONST from 'constants.js';

class AudioTrackPlayback extends Component {
  constructor(props) {
    super(props);
    this.setting = {
      duckPercent: 10,
      fadeTimeInSeconds: 1,
      fadeNumberOfSteps: 1000,
      secondsBeforeRepeatAllowed: 10
    };

    let playerDefault = {
      fadeDirection: CONST.FADE.UP,
      volume: 0,
      isPlaying: false,
      isFading: false,
      isLooping: false,
      currentTrack: null
    };
    this.defaultState = {
      currentPrimary: CONST.PLAYER.PRIMARY,
      lastTriggerAt: null,
      lastTriggerID: null,
      transition: null,
      queue: [],
      player: [{ ...playerDefault }, { ...playerDefault }]
    };
    this.state = { ...this.defaultState };
  }

  onReset = () => {
    if (this.timer) {
      clearInterval(this.timer[CONST.PLAYER.PRIMARY]);
      clearInterval(this.timer[CONST.PLAYER.SECONDARY]);
    }
    this.setState({
      ...this.defaultState,
      lastTriggerAt: this.state.lastTriggerAt,
      lastTriggerID: this.state.lastTriggerID
    });
  };

  componentDidMount() {
    window.addEventListener('audioTourTrigger', this.onIncomingTrigger);
    window.addEventListener('audioTourReset', this.onReset);
  }

  componentWillUnmount() {
    window.removeEventListener('audioTourTrigger', this.onIncomingTrigger);
    window.removeEventListener('audioTourReset', this.onReset);
  }

  onIncomingDuck = () => {
    console.error("I don't know how to handle DUCKING yet");
  };

  onIncomingDuck = () => {
    console.error("I don't know how to handle UN-DUCKING yet");
  };

  onIncomingTrigger = (e) => {
    if (!e.detail[this.props.trackID]) return;

    const triggerID = e.detail[this.props.trackID][0].audiotour_id;
    const secondsSinceLast = Math.round(
      (new Date() - this.state.lastTriggerAt) / 1000
    );
    const triggerAllowed =
      this.state.lastTriggerID !== triggerID ||
      secondsSinceLast >= this.setting.secondsBeforeRepeatAllowed;

    if (triggerAllowed) {
      let transition = null;
      const queue = [];
      Object.values(e.detail[this.props.trackID]).forEach((item, idx) => {
        if (idx === 0) transition = item.transition_id;
        queue.push(item);
      });
      this.setState({
        lastTriggerID: triggerID,
        lastTriggerAt: Date.now(),
        queue,
        transition
      });
      this._onPlayNextInQueue();
    }
  };

  _onPlayNextInQueue = () => {
    if (this.state.queue.length <= 0) {
      console.warn('Queue is empty!');
    } else {
      let queue = [...this.state.queue];
      let primaryOccupied = this.state.player[this.state.currentPrimary]
        .isPlaying;
      switch (this.state.transition) {
        case CONST.TRANSITION.FADE: {
          let primaryPlayer = this.state.currentPrimary;
          let secondaryPlayer =
            this.state.currentPrimary === CONST.PLAYER.PRIMARY
              ? CONST.PLAYER.SECONDARY
              : CONST.PLAYER.PRIMARY;
          let currentTrack = queue.shift();
          if (primaryOccupied) {
            this.setState({
              ...this.state,
              queue,
              currentPrimary: secondaryPlayer,
              player: {
                ...this.state.player,
                [primaryPlayer]: {
                  ...this.state.player[primaryPlayer],
                  isFading: true,
                  fadeDirection: CONST.FADE.DOWN
                },
                [secondaryPlayer]: {
                  ...this.state.player[secondaryPlayer],
                  currentTrack,
                  isPlaying: true,
                  isFading: true,
                  fadeDirection: CONST.FADE.UP,
                  volume: 0
                }
              }
            });
          } else {
            this.setState({
              ...this.state,
              queue,
              player: {
                ...this.state.player,

                [secondaryPlayer]: {
                  ...this.state.player[secondaryPlayer],
                  isPlaying: false,
                  isFading: false,
                  fadeDirection: CONST.FADE.DOWN
                },
                [primaryPlayer]: {
                  ...this.state.player[secondaryPlayer],
                  currentTrack,
                  isPlaying: true,
                  isFading: true,
                  fadeDirection: CONST.FADE.UP,
                  volume: 0
                }
              }
            });
          }
          break;
        }
        case CONST.TRANSITION.DEVAMP:
        case CONST.TRANSITION.CUT: {
          this.onReset();
          let currentTrack = queue.shift();
          let playerID = CONST.PLAYER.PRIMARY;
          this.setState({
            ...this.state,
            queue,
            player: {
              ...this.state.player,
              [playerID]: {
                ...this.state.player[playerID],
                currentTrack: currentTrack,
                isPlaying: true,
                isFading: false,
                volume: 1
              }
            }
          });
          break;
        }
        default:
          console.log(
            `No idea how to manage transition: ${this.state.transition}`
          );
      }
    }
  };

  fade = (playerID) => {
    if (!this.timer) this.timer = [];
    clearInterval(this.timer[playerID]);
    this.timer[playerID] = setInterval(() => {
      let volStep = 1 / this.setting.fadeNumberOfSteps;
      let volume;
      if (this.state.player[playerID].fadeDirection === CONST.FADE.UP) {
        volume = this.state.player[playerID].volume + volStep;
      } else {
        volume = this.state.player[playerID].volume - volStep;
      }
      volume = volume > 1 ? 1 : volume < 0 ? 0 : volume;

      let isFinished =
        (this.state.player[playerID].fadeDirection === CONST.FADE.UP &&
          volume >= 1) ||
        (this.state.player[playerID].fadeDirection === CONST.FADE.DOWN &&
          volume <= 0);

      this.setState({
        ...this.state,
        player: {
          ...this.state.player,
          [playerID]: {
            ...this.state.player[playerID],
            volume: volume,
            isFading: !isFinished
          }
        }
      });
      if (isFinished) {
        //clearInterval(this.timer[playerID]);
      }
    }, (this.setting.fadeTimeInSeconds / this.setting.fadeNumberOfSteps) * 1000);
  };

  _onEndPlay = () => {
    this.setState({
      ...this.state,
      player: {
        ...this.state.player,
        [this.state.currentPrimary]: {
          ...this.state.player[this.state.currentPrimary],
          isPlaying: false,
          currentTrack: null
        }
      }
    });
    if (this.state.queue.length > 0) {
      console.log(`${this.state.queue.length} items in queue, playing next`);
      this._onPlayNextInQueue();
    }
  };

  _onStartPlay = () => {
    this.fade(CONST.PLAYER.PRIMARY);
    this.fade(CONST.PLAYER.SECONDARY);
  };

  render() {
    let isPlaying =
      !missingValue(
        this.state.player[this.state.currentPrimary].currentTrack
      ) &&
      !missingValue(
        this.state.player[this.state.currentPrimary].currentTrack.media_url
      );

    return (
      <div className={styles.container_player} key={this.props.trackID}>
        <div>
          <span
            style={{
              color: isPlaying ? '#a0ff7a' : 'red',
              margin: '0 1rem 0 0',
              fontSize: '1rem'
            }}
            className={isPlaying ? 'fas fa-play' : 'fas fa-stop'}
          />
          Track: {this.props.trackID} ({this.state.queue.length})<br />
          {[...Array(2)].map((_, playerID) => {
            return (
              <div
                key={`player_${this.props.trackID}_${playerID}`}
                style={{
                  opacity: this.state.currentPrimary === playerID ? 1 : 0.5
                }}
              >
                <div className={styles.row} style={{ marginTop: '1rem' }}>
                  <div
                    style={{
                      color: 'white',
                      margin: '0 1rem 0 0',
                      fontSize: '1rem'
                    }}
                    className={
                      this.state.player[playerID].isFading
                        ? this.state.player[playerID].fadeDirection ===
                          CONST.FADE.UP
                          ? 'fas fa-arrow-up'
                          : 'fas fa-arrow-down'
                        : 'fas fa-volume-off'
                    }
                  />
                  <div>{this.state.player[playerID].volume.toFixed(2)}</div>
                </div>

                <div className={styles.row}>
                  <div>
                    {!isPlaying && <div>* SILENCE *</div>}
                    {isPlaying && (
                      <ReactPlayer
                        volume={this.state.player[playerID].volume}
                        playing={this.state.player[playerID].isPlaying}
                        height="2rem"
                        onStart={() => this._onStartPlay(playerID)}
                        onEnded={() => this._onEndPlay(playerID)}
                        width="15.2rem"
                        url={
                          this.state.player[playerID].currentTrack
                            ? makeAssetURL(
                                this.state.player[playerID].currentTrack
                                  .media_url
                              )
                            : null
                        }
                        controls
                      />
                    )}
                  </div>
                  <div
                    style={{
                      color: 'yellow',
                      margin: '0 0 0 1rem',
                      fontSize: '1rem'
                    }}
                    className={
                      this.state.currentPrimary === playerID
                        ? 'fas fa-arrow-circle-left'
                        : ''
                    }
                  />
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
export default AudioTrackPlayback;

AudioTrackPlayback.defaultProps = {
  trackID: 0
};
AudioTrackPlayback.propTypes = {
  trackID: PropTypes.number.isRequired
};
