import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './AudioTour.module.css';

class AudioTourTrack extends Component {
  render() {
    return (
      <React.Fragment>
        {this.props.title}
        <div className={styles.row}>
          <div className={styles.label}>Name:</div>
          <div>
            {this.props.trigger !== null ? this.props.trigger.poi.name : ''}
          </div>
        </div>
        <div className={styles.row}>
          <div className={styles.label}>Fired:</div>
          <div>
            {this.props.trigger !== null
              ? `${Math.floor(
                  (new Date() - this.props.trigger.firedAt) / 1000
                )} seconds ago`
              : ''}
          </div>
        </div>

        {[...Array(this.props.numberOfTracks)].map((_, trackID) => {
          let trackInfo = [];
          if (this.props.trigger) {
            if (this.props.trigger?.trackState[trackID]) {
              Object.values(this.props.trigger.trackState[trackID]).forEach(
                (trackPart, idx) => {
                  trackInfo.push(
                    <div key={`track_${trackID}_${idx}`}>
                      <div>
                        {trackPart.sortorder === 0
                          ? `${trackPart.transition} to `
                          : ', '}
                        {trackPart.media_name}
                      </div>
                    </div>
                  );
                }
              );
            }
          }

          return (
            <div className={styles.row} key={`track_${trackID}`}>
              <div className={styles.label}>Track {trackID}:</div>
              <div>{trackInfo}</div>
            </div>
          );
        })}
      </React.Fragment>
    );
  }
}
export default AudioTourTrack;

AudioTourTrack.defaultProps = {
  title: 'Track',
  numberOfTracks: 0
};
AudioTourTrack.propTypes = {
  title: PropTypes.string,
  trigger: PropTypes.object,
  numberOfTracks: PropTypes.number
};
