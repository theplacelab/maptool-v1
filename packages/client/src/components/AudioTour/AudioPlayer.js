import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './AudioTour.module.css';
import ReactPlayer from 'react-player';

class AudioPlayer extends Component {
  constructor(props) {
    super(props);
    let queue = [];
    [...Array(this.props.numberOfTracks)].map((_, trackID) => {
      queue[trackID] = [];
    });
    this.state = {
      track: [
        {
          isPlaying: false,
          queue: []
        }
      ]
    };
  }

  componentDidMount() {
    window.addEventListener('audioTourTrigger', this.onIncomingTrigger);
  }

  componentWillUnmount() {
    window.removeEventListener('audioTourTrigger', this.onIncomingTrigger);
  }

  onIncomingTrigger = (e) => {
    console.log(e.detail);
  };

  render() {
    //console.log(this.state.queue);
    return (
      <div className={styles.container_player}>
        {[...Array(this.props.numberOfTracks)].map((_, trackID) => {
          return (
            <div key={trackID}>
              Track: {trackID}
              <span
                style={{
                  color: '#a0ff7a',
                  margin: '0 0 0 1rem',
                  fontSize: '1rem'
                }}
                className={
                  this.state.track.isPlaying ? 'fas fa-play' : 'fas fa-stop'
                }
              />
              <ReactPlayer
                playing={true}
                height="2rem"
                width="15.2rem"
                url={null}
                controls
              />
            </div>
          );
        })}
      </div>
    );
  }
}
export default AudioPlayer;

AudioPlayer.propTypes = {
  numberOfTracks: PropTypes.number.isRequired
};
