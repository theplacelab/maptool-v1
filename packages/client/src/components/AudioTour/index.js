import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './AudioTour.module.css';
import { exportDataToFile, missingValue } from 'utility';
import turfDistance from '@turf/distance';
import turfBearing from '@turf/bearing';
import { point } from '@turf/helpers';
import memoizeOne from 'memoize-one';
import AudioTourTrack from './AudioTourTrack';
import AudioTrackPlayback from './AudioTrackPlayback';
import BearingArrow from './BearingArrow';
import { Button, Collapsable } from 'components';

class AudioTour extends Component {
  constructor(props) {
    super(props);
    this.defaultState = {
      coordinates: '',
      bounds: '',
      zoom: '',
      start: null,
      insideStart: false,
      insideNearest: false,
      bearingToStart: 0,
      bearingToNearest: 0,
      nearest: { distance: 0, poi: { name: '' } },
      inside: null,
      lastTrigger: null,
      priorTrigger: null,
      poiByDistance: [],
      dwellTime: 0,
      gps_use: false,
      gps_coordinates: null
    };

    this.state = { ...this.defaultState };

    this.m_updateGPSLocation = memoizeOne((position) => {
      if (position === null) return;
      if (this.state.gps_use) {
        let gps_coordinates = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        this.setState({ gps_coordinates });
        this.onLocationChange(gps_coordinates);
      }
    });

    this.m_getTriggerFor = memoizeOne((inside) => {
      if (!inside) return;
      if (this.state.lastTrigger !== null) {
        if (inside.poi.id !== this.state.lastTrigger.poi.id)
          this.setState({
            priorTrigger: { ...this.state.lastTrigger }
          });
      }
      let lastTrigger = { ...inside, firedAt: Date.now() };
      this.setState({ lastTrigger });
      if (lastTrigger.trackState)
        window.dispatchEvent(
          new CustomEvent('audioTourTrigger', {
            detail: Object.values(lastTrigger.trackState)
          })
        );
    });
  }

  onReset = () => {
    this.setState(this.defaultState);
    window.dispatchEvent(new CustomEvent('audioTourReset'));
  };

  componentDidMount = () => {
    window.addEventListener('coordinateUpdate', this.onPointerLocationChange);
    window.addEventListener('mapBoundsChanged', this.onMapBoundsChange);
    this.onMapBoundsChange();
    setInterval(() => {
      this.setState({ dwellTime: this.state.dwellTime + 1 });
    }, 1000);
  };

  componentWillUnmount = () => {
    window.removeEventListener('mapBoundsChanged', this.onMapBoundsChange);
    window.removeEventListener(
      'coordinateUpdate',
      this.onPointerLocationChange
    );
  };

  componentDidUpdate = () => {
    this.m_updateGPSLocation(this.props.locationState.currentLocation);
  };

  sortPoiByDistanceFrom = (currentLocation) => {
    let sorted = [];
    this.props.poi.forEach((location) => {
      const distance = turfDistance(
        point([currentLocation.lat, currentLocation.lng]),
        point([location.coordinate.lat, location.coordinate.lng]),
        { units: 'meters' }
      );
      sorted.push({
        poi: location,
        trackState: this.props.config?.location
          ? this.props.config?.location[location.id]
          : null,
        id: location.id,
        inside: location.representation_config.options
          ? distance <= location.representation_config.options.radius
          : false,
        distance
      });
    });

    sorted.sort((a, b) =>
      a.distance > b.distance ? 1 : b.distance > a.distance ? -1 : 0
    );

    return sorted;
  };

  onPointerLocationChange = (e) => {
    if (!this.state.gps_use) this.onLocationChange(e.coordinates);
  };

  onLocationChange = (coordinates) => {
    const poiByDistance = this.sortPoiByDistanceFrom(coordinates);
    const nearest = poiByDistance[0];
    const start = poiByDistance.find((loc) => parseInt(loc.poi.date, 10) === 1);
    const bearingToNearest = nearest
      ? turfBearing(
          point([coordinates.lat, coordinates.lng]),
          point([nearest.poi.coordinate.lat, nearest.poi.coordinate.lng])
        )
      : null;

    const bearingToStart = start
      ? turfBearing(
          point([coordinates.lat, coordinates.lng]),
          point([start.poi.coordinate.lat, start.poi.coordinate.lng])
        )
      : null;
    const insideStart = start
      ? nearest.inside && start.poi.id === nearest.poi.id
      : false;

    this.setState({
      dwellTime: 0,
      start,
      insideStart,
      insideNearest: nearest ? nearest.inside : null,
      bearingToStart,
      bearingToNearest,
      nearest,
      inside: nearest && nearest.inside ? nearest : null,
      poiByDistance,
      coordinates: coordinates,
      bounds: JSON.stringify(this.props.mapObject.getBounds()),
      zoom: this.props.mapObject.getZoom()
    });

    this.m_getTriggerFor(this.state.inside);
  };

  onMapBoundsChange = () => {
    if (this.props.mapObject !== null) {
      this.setState({
        center: JSON.stringify(this.props.mapObject.getBounds().getCenter()),
        bounds: JSON.stringify(this.props.mapObject.getBounds()),
        zoom: this.props.mapObject.getZoom()
      });
    }
  };

  render() {
    let exportGPX = () => {
      this.exportGPX(
        `${JSON.parse(this.state.center)['lat']}_${
          JSON.parse(this.state.center)['lng']
        }`,
        JSON.parse(this.state.center)['lat'],
        JSON.parse(this.state.center)['lng']
      );
    };

    let exportTourConfig = () => {
      exportDataToFile(
        JSON.stringify(this.props.config),
        `tourconfig.json`,
        false
      );
    };

    if (this.props.mapObject !== null) {
      return (
        <React.Fragment>
          <div className={styles.container}>
            <div className={styles.row}>
              <Button
                style={{
                  width: '100%',
                  color: 'black',
                  margin: '1rem 0 1rem 0'
                }}
                onClick={exportTourConfig}
              >
                Export TourConfig
              </Button>
              <Button
                style={{
                  width: '100%',
                  color: 'black',
                  margin: '1rem 0 1rem 0'
                }}
                onClick={this.onReset}
              >
                Reset
              </Button>
            </div>

            <div className={styles.row}>
              <div className={styles.label}>Use GPS:</div>
              <div>
                <input
                  style={{ zoom: '150%' }}
                  type="checkbox"
                  checked={this.state.gps_use}
                  onChange={(e) => {
                    this.setState({ gps_use: e.target.checked });
                  }}
                />
              </div>
            </div>
            {this.state.gps_use && (
              <div>
                <input
                  readOnly
                  type="text"
                  value={JSON.stringify(this.state.gps_coordinates)}
                />
              </div>
            )}

            <div>
              <div>
                <div>Dwell:</div>
                <div>
                  <input
                    readOnly
                    type="text"
                    value={`${this.state.dwellTime} seconds`}
                  />
                </div>
              </div>
              {!missingValue(this.state.start) && (
                <div>
                  <div>Start:</div>
                  <div className={styles.row}>
                    <input
                      readOnly
                      type="text"
                      value={`(${this.state.start.distance.toFixed(
                        2
                      )} meters) ${this.state.start.poi.name}`}
                    />
                    <BearingArrow
                      showArrow={!this.state.insideStart}
                      distanceToDestination={this.state.start.distance}
                      bearing={this.state.bearingToStart}
                    />
                  </div>
                </div>
              )}
              {this.state.nearest && (
                <React.Fragment>
                  <div>Nearest:</div>
                  <div className={styles.row}>
                    <input
                      readOnly
                      type="text"
                      value={`(${this.state.nearest.distance.toFixed(
                        2
                      )} meters) ${this.state.nearest.poi.name}`}
                    />
                    <BearingArrow
                      showArrow={!this.state.insideNearest}
                      distanceToDestination={this.state.nearest.distance}
                      bearing={this.state.bearingToNearest}
                    />
                  </div>
                </React.Fragment>
              )}
            </div>
            <div>
              <div>Currently Inside:</div>
              <div>
                <input
                  readOnly
                  type="text"
                  value={
                    this.state.inside !== null
                      ? this.state.inside.poi.name
                      : '-'
                  }
                />
              </div>
            </div>

            <hr />
            <AudioTourTrack
              startPlay={true}
              title="Most Recent"
              trigger={this.state.lastTrigger}
              numberOfTracks={this.props.numberOfTracks}
            />
            <hr />
            <AudioTourTrack
              startPlay={false}
              title="Prior"
              trigger={this.state.priorTrigger}
              numberOfTracks={this.props.numberOfTracks}
            />

            <hr />
            {!this.state.gps_use && (
              <Collapsable title="GPX">
                <div className={styles.row}>
                  <div className={styles.label}>Coordinates:</div>
                  <div>
                    <input
                      readOnly
                      type="text"
                      value={this.state.coordinates}
                    />
                  </div>
                </div>
                <div className={styles.row}>
                  <div className={styles.label}>Bounds:</div>
                  <div>
                    <input readOnly type="text" value={this.state.bounds} />
                  </div>
                </div>
                <div className={styles.row}>
                  <div className={styles.label}>Zoom:</div>
                  <div>
                    <input readOnly type="text" value={this.state.zoom} />
                  </div>
                </div>

                <Button
                  style={{
                    width: '95%',
                    color: 'black',
                    margin: '1rem 0 1rem 0'
                  }}
                  onClick={exportGPX}
                >
                  Export GPX
                </Button>
              </Collapsable>
            )}
            <AudioTrackPlayback trackID={0} />
            <AudioTrackPlayback trackID={1} />
            <AudioTrackPlayback trackID={2} />
          </div>
        </React.Fragment>
      );
    } else {
      return null;
    }
  }

  exportGPX = (name, lat, lng) => {
    let data = `<?xml version="1.0"?><gpx version="1.1" creator="Xcode"><wpt lat="${lat}" lon="${lng}"><name>${name}</name></wpt></gpx>`;
    exportDataToFile(data, `${name}.gpx`, false);
  };
}
export default AudioTour;

AudioTour.propTypes = {
  config: PropTypes.array,
  numberOfTracks: PropTypes.number,
  locationState: PropTypes.object,
  isLocationEnabled: PropTypes.bool,
  poi: PropTypes.array.isRequired,
  mapObject: PropTypes.object.isRequired
};
