import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Collapsable } from 'components';
import CONST from 'constants.js';
import ConfigureTrack from './ConfigureTrack.js';
import { useSelector, useDispatch } from 'react-redux';
import * as actions from 'state/actions';
import colors from 'colors';
import { Sortable } from 'components/DragNDrop';
import uuidv4 from 'uuid/v4';

const Proximity = () => {
  const dragGroupId = uuidv4();
  const dispatch = useDispatch();
  const tourSettings = useSelector((redux) => redux.audioTour);
  const availableProximity = useSelector((redux) => redux.proximity.all);

  const numberOfTracks = useSelector(
    (redux) => redux.app.prefs.numberOfTracks.value
  );
  const [sortableItems, setSortableItems] = useState(availableProximity);
  if (!sortableItems) return null;

  const onChange = (e) => {
    dispatch(
      actions.updateAudiotourItem({
        ...e.data.trackItem,
        [e.id]: e.value
      })
    );
  };

  const onChangeGroup = (e) => {
    let payload =
      e.id === 'transition_options'
        ? {
            ...e.data.trackItem,
            transition_options: {
              ...e.data.trackItem.transition_options,
              [e.data.option]: e.value
            }
          }
        : {
            ...e.data.trackItem,
            [e.id]: e.value
          };
    if (payload.isNew) {
      dispatch(actions.createAudiotourItem(payload));
    } else {
      dispatch(actions.updateAudiotourItemGroup(payload));
    }
  };

  let form = [];
  sortableItems.forEach((sortable, idx) => {
    const proximity = availableProximity.find(
      (item) => item.id === sortable.id
    );
    if (!proximity) return null;
    let subForm = [];
    for (let trackId = 0; trackId < numberOfTracks; trackId++) {
      subForm.push(
        <ConfigureTrack
          key={`${proximity.id}_${trackId}`}
          trackId={trackId}
          trackItems={Object.values(tourSettings.items)
            .filter(
              (track) =>
                track.trigger_type_id === CONST.TRIGGER.LOCATION &&
                track.track_id === trackId &&
                track.proximity_id === proximity.id
            )
            .sort((a, b) => a.sortorder - b.sortorder)}
          availableProximity={availableProximity}
          //availableTod={availableTod}
          //availableWeather={availableWeather}
          onChange={onChange}
          onChangeGroup={onChangeGroup}
          onAddFollow={(e) =>
            dispatch(
              actions.createAudiotourItem(
                JSON.parse(e.currentTarget.dataset.meta)
              )
            )
          }
          onRemoveFollow={(e) =>
            dispatch(actions.deleteAudiotourItem(e.currentTarget.dataset.meta))
          }
        />
      );
    }
    form.push(
      <Sortable
        accepts={dragGroupId}
        key={proximity.id}
        index={idx}
        id={proximity.id}
        sortableItems={sortableItems}
        setSortableItems={setSortableItems}
        onChange={(proximityArr) => {
          dispatch(
            actions.resequenceProximity(
              proximityArr.map((item, idx) => ({
                id: item.id,
                sortorder: idx
              }))
            )
          );
        }}
      >
        <div>
          <Collapsable
            style={{ backgroundColor: colors.primary, border: '0px' }}
            key={`location_trigger_${proximity.id}`}
            title={
              <div style={styles.proximityBar}>
                <div style={styles.proximityName}>
                  [{proximity.sortorder + 1}] {proximity.name}
                </div>
                <div style={styles.proximityIcons} />
              </div>
            }
            isOpen={idx === 0}
          >
            {subForm}
          </Collapsable>
        </div>
      </Sortable>
    );
  });
  return (
    <div style={styles.container}>
      <div style={styles.pageTitle}>
        ({availableProximity.length}) Proximity Triggers
      </div>
      <div>{form}</div>
    </div>
  );
};

const styles = {
  container: { padding: '0 1rem 1rem 1rem' },
  pageTitle: { marginBottom: '1rem', fontSize: '1.5rem' },
  proximityBar: {
    padding: '0.5rem',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%'
  },
  proximityName: { fontSize: '1.2rem', flexGrow: 1 },
  proximityIcons: { width: '5rem' }
};

export default Proximity;

Proximity.defaultProps = {
  scrollTop: 0,
  showArrow: true,
  resetToTop: false
};

Proximity.propTypes = {
  items: PropTypes.array
};
