import initialState from './init/playback';
//import * as actions from 'state/actions';

export default function app(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
