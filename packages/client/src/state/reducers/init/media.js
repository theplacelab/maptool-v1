export default {
  needsUpdate: [],
  available: {
    media: null,
    triggers: null,
    tracks: null,
    transitions: null
  }
};
