export default {
  current: null,
  all: [],
  filtered: [],
  selection_options: [],
  needsInsert: [],
  needsUpdate: [],
  needsRemoval: []
};
