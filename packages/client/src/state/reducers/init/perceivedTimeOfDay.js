export default [
  { id: 0, name: 'EARLY_MORNING' },
  { id: 1, name: 'MORNING' },
  { id: 2, name: 'DAY' },
  { id: 3, name: 'EVENING' },
  { id: 4, name: 'NIGHT' }
];
