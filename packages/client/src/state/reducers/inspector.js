import initialState from './init/inspector';
import * as actions from 'state/actions';

export default function app(state = initialState, action) {
  switch (action.type) {
    case actions.APP_PREVIEW_INSPECTOR:
      return { ...state, isPreview: action.payload };
    default:
      return state;
  }
}
