import * as CONSTANTS from '../constants';
document.addEventListener(
  'gate_enter',
  (e) => {
    if (window.APP_MODE === CONSTANTS.MODE.EXPLORE) {
      console.log(e.detail);
    }
  },
  false
);

document.addEventListener(
  'gate_exit',
  (e) => {
    if (window.APP_MODE === CONSTANTS.MODE.EXPLORE) {
      console.log(e.detail);
    }
  },
  false
);
