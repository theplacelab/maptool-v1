import UTM from 'utm-latlng';
const utm = new UTM();

export const setup = (config) => {
  let ul_coordinateUTM = utm.convertLatLngToUtm(
    config.upperLeft_geoCoordinate.lat,
    config.upperLeft_geoCoordinate.lng,
    1
  );
  let lr_coordinateUTM = utm.convertLatLngToUtm(
    config.lowerRight_geoCoordinate.lat,
    config.lowerRight_geoCoordinate.lng,
    1
  );

  const scalingFactor = {
    x:
      config.mapDimensionsInPx.width /
      Math.abs(lr_coordinateUTM.Easting - ul_coordinateUTM.Easting),
    y:
      config.mapDimensionsInPx.height /
      Math.abs(lr_coordinateUTM.Northing - ul_coordinateUTM.Northing)
  };

  let mapConfig = { ...config, scalingFactor };
  return {
    ...mapConfig,
    convert: (coordinate) => {
      return convertLanLngToPx(coordinate, mapConfig);
    }
  };
};

export const convertLanLngToPx = (coordinate, config) => {
  let inputCoordinateUTM = utm.convertLatLngToUtm(
    coordinate.lat,
    coordinate.lng,
    1
  );

  const ul_coordinateUTM = utm.convertLatLngToUtm(
    config.upperLeft_geoCoordinate.lat,
    config.upperLeft_geoCoordinate.lng,
    1
  );
  let y =
    (inputCoordinateUTM.Northing - ul_coordinateUTM.Northing) *
    config.scalingFactor.x *
    -1;
  y = Object.is(y, -0) ? 0 : y;
  return {
    x:
      (inputCoordinateUTM.Easting - ul_coordinateUTM.Easting) *
      config.scalingFactor.y,
    y: y
  };
};
