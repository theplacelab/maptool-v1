import { rest } from 'msw';
//console.log(`${process.env.REACT_APP_JWT_SECRET}`);
export const handlers = [
  rest.post(
    'example.com', //`${window._env_.REACT_APP_BASE_URL}/api/rpc/login`,
    (req, res, ctx) => {
      return res(
        ctx.json([
          {
            token:
              'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYUBmZXJhbHJlc2VhcmNoLm9yZyIsImdyb3VwX3JvbGUiOiJ3ZWJzdXBlciIsImVtYWlsIjoiYUBmZXJhbHJlc2VhcmNoLm9yZyIsImV4cCI6MTU5MzcyNTAwMywibmFtZSI6IkFuZHJldyIsInBpY3R1cmUiOiJodHRwczovL2Fzc2V0cy50aGVwbGFjZWxhYi5jb20vZGVtby91c2VyLnBuZyIsImlzX3ZhbGlkYXRlZCI6dHJ1ZSwiZm9yY2VfbmV3cGFzcyI6dHJ1ZSwiaWQiOjF9.g_xGIHOu1HDmO-ASJ-V8HoY27uheo0mVlDhs0lFER4E'
          }
        ]),
        ctx.status(200)
      );
    }
  )
];
