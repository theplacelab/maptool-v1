'use strict';
require('dotenv').config({ path: require('find-config')('.env') });
const middleware = require('./src/middleware.js');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const util = require('./src/util.js');
const route_user = require('./src/route_user.js');
const route_s3 = require('./src/route_s3.js');
const route_packager = require('./src/route_packager.js');
const sharedState = require('./src/sharedState.js');
const errorhandler = require('errorhandler');
const app = express();
const mailClient = route_user.initMailClient();
sharedState.isInProgress = false;

if (!util.environmentOK()) {
  console.error('FATAL: Configuration problem');
  app.all('*', (req, res) => {
    res.sendStatus(500);
  });
} else {
  util.log(`\n--------------------------------------`);
  util.log(`Maptool Server`);
  util.log(`Port: ${process.env.PORT || 8888}`);
  util.log(`--------------------------------------`);

  process.on('uncaughtException', function(exception) {
    console.log(exception);
  });

  if (process.env.NODE_ENV === 'development') {
    // only use in development
    app.use(errorhandler({ dumpExceptions: true, showStack: true }));
  }

  app.use(cors());
  app.use(bodyParser.json());
  app.engine('html', require('ejs').renderFile);
  app.listen(process.env.PORT || 8888);

  route_packager.updateLibStats();

  // General
  app.get('/info', middleware.verifyToken, (req, res) => {
    util.log(`INFO: ${req.method}:${req.originalUrl}`);
    route_s3.info(req, res);
  });

  // Use
  app.get('/user/request_validation', middleware.verifyToken, (req, res) => {
    route_user.request_validation(req, res, {
      client: mailClient
    });
  });
  app.get(
    '/user/request_validation/:user_id',
    middleware.verifyToken,
    (req, res) => {
      route_user.request_validation(req, res, {
        client: mailClient,
        user_id: req.params.user_id
      });
    }
  );
  app.get('/user/send_test_email', middleware.verifyToken, (req, res) => {
    route_user.send_test_email(req, res, {
      client: mailClient
    });
  });
  app.get('/user/validate/:validation_token', (req, res) => {
    route_user.validate(req, res, {
      validation_token: req.params.validation_token
    });
  });
  app.get('/user/reset_password/:user_id', (req, res) => {
    route_user.resetPassword(req, res, {
      client: mailClient,
      user_id: req.params.user_id
    });
  });

  // S3 Function
  app.get('/sign-s3', middleware.verifyToken, (req, res) => {
    util.log(`SIGN: ${req.method}:${req.originalUrl}`);
    route_s3.signS3(req, res);
  });
  app.post('/delete', middleware.verifyToken, (req, res) => {
    util.log(`DELETE: ${req.method}:${req.originalUrl}`);
    route_s3.delete(req, res);
  });
  app.get('/delete/:id', middleware.verifyToken, (req, res) => {
    util.log(`DELETE: ${req.method}:${req.originalUrl}`);
    route_s3.delete_id(req, res);
  });

  //Packager
  app.get('/package/:action', (req, res) => {
    middleware.verifyWithFallback(
      req,
      res,
      () => route_packager.secureGET(req, res),
      () => route_packager.anonGET(req, res)
    );
  });
  app.get('/package/:action/:uuid', (req, res) => {
    middleware.verifyWithFallback(
      req,
      res,
      () => route_packager.secureGET(req, res),
      () => route_packager.anonGET(req, res)
    );
  });

  // Fallthrough
  app.use(function(req, res) {
    util.log(`REJECTING: ${req.method}:${req.originalUrl}`);
    res.sendStatus(404);
  });
}
