const path = require('path');
const fs = require('fs-extra');
const axios = require('axios');
const rimraf = require('rimraf');
const archiver = require('archiver');
const cloudflare = require('./cloudflare.js');
const s3Helper = require('./s3.js');
const sharedState = require('./sharedState.js');
const globalManifest = `${process.env.APP_ID}/${process.env.APP_PACKAGE_FOLDER}/manifest.json`;
const AWS = require('aws-sdk');
AWS.config.region = process.env.AWS_REGION || 'us-east-1';
const s3 = new AWS.S3({
  endpoint: new AWS.Endpoint(process.env.AWS_ENDPOINT)
});

const archive = (fileName, inputDir, outputDir, cb) => {
  const archive = archiver('zip', { zlib: { level: 9 } });
  const archivePath = path.join(outputDir, `${fileName}.zip`);
  const output = fs.createWriteStream(archivePath);
  archive.pipe(output);
  archive.directory(inputDir, false);
  archive.on('error', (err) => console.log(err));
  archive.on('progress', (event) => (sharedState.zipStatus = event));
  output.on('close', () => {
    cb(archive.pointer());
  });
  archive.finalize();
};

const deletePackage = (uuid) => {
  inProgress(true, uuid);
  s3Helper.deleteFiles([
    `${process.env.APP_ID}/${process.env.APP_PACKAGE_FOLDER}/${uuid}.zip`
  ]);
  s3Helper.getJSONFrom(
    process.env.AWS_BUCKET,
    globalManifest,
    (existingManifest) => {
      const manifestContent = existingManifest.filter(
        (pkg) => pkg.uuid !== uuid
      );
      rewriteManifest(uuid, manifestContent, () => {
        inProgress(false, uuid);
      });
    }
  );
};

const createPackage = (uuid) => {
  inProgress(true, uuid);
  const outputDir = path.join(__dirname, `../TMP/${uuid}`);
  const archiveDir = path.join(__dirname, `../TMP`);
  const baseUrl = `${process.env.REACT_APP_BASE_URL}/api`;
  let manifest = {
    package_date: Date.now(),
    progressPercent: 0,
    uuid,
    size: 0,
    count: 0
  };
  axios
    .all([
      axios.get(`${baseUrl}/mediapermap?map_uuid=eq.${uuid}`),
      axios.get(`${baseUrl}/audiotour?map_uuid=eq.${uuid}`),
      axios.get(`${baseUrl}/audiotour_config?map_uuid=eq.${uuid}`),
      axios.get(`${baseUrl}/config?uuid=eq.${uuid}`),
      axios.get(`${baseUrl}/poi_with_data?map_uuid=eq.${uuid}`),
      axios.get(`${baseUrl}/proximity?map_uuid=eq.${uuid}`)
    ])
    .then((responseArr) => {
      rimraf(outputDir, (err) => {
        if (err) {
          console.log(err);
        } else {
          fs.mkdir(outputDir, { recursive: true }, (err) => {
            if (err) throw err;
            const media = responseArr[0].data;
            const audioTour = responseArr[1].data;
            const audioTourConfig = responseArr[2].data;
            const config = responseArr[3].data[0];
            const poiData = responseArr[4].data;
            const proximityData = responseArr[5].data;
            fs.writeFile(`${outputDir}/media.json`, JSON.stringify(media));
            fs.writeFile(`${outputDir}/config.json`, JSON.stringify(config));
            fs.writeFile(
              `${outputDir}/audioTour.json`,
              JSON.stringify(audioTour)
            );
            fs.writeFile(
              `${outputDir}/audioTourConfig.json`,
              JSON.stringify(audioTourConfig)
            );
            fs.writeFile(`${outputDir}/poi.json`, JSON.stringify(poiData));
            fs.writeFile(
              `${outputDir}/proximity.json`,
              JSON.stringify(proximityData)
            );
            let packageSize = 0;
            let itemsProcessed = 0;

            const closePackage = (manifest) => {
              fs.writeFile(
                `${outputDir}/manifest.json`,
                JSON.stringify(manifest)
              );
              s3Helper.getJSONFrom(
                process.env.AWS_BUCKET,
                globalManifest,
                (existingManifest) => {
                  const manifestContent = existingManifest.filter(
                    (pkg) => pkg.uuid !== uuid
                  );

                  archive(uuid, outputDir, archiveDir, (archiveSize) => {
                    let globalManifestEntry = {
                      ...manifest,
                      archiveSize,
                      title: config.title
                    };
                    delete globalManifestEntry.list;
                    manifestContent.push(globalManifestEntry);
                    rewriteManifest(uuid, manifestContent, () => {
                      let params = {
                        Bucket: process.env.AWS_BUCKET,
                        Key: `${process.env.APP_ID}/${process.env.APP_PACKAGE_FOLDER}/${uuid}.zip`,
                        ContentType: 'application/zip'
                      };
                      fs.readFile(
                        `${archiveDir}/${uuid}.zip`,
                        (err, fileData) => {
                          s3.putObject({ ...params, Body: fileData }, () => {
                            inProgress(false, uuid);
                            rimraf(archiveDir, (err) => {
                              if (err) console.log(err);
                            });
                          });
                        }
                      );
                    });
                  });
                }
              );
            };

            if (!media || media.length === 0) {
              closePackage({
                ...manifest,
                list: media
              });
            } else {
              for (const file of media) {
                s3.getObject(
                  {
                    Bucket: process.env.AWS_BUCKET,
                    Key: `${process.env.APP_ID}/${file.url}`
                  },
                  (err, data) => {
                    if (err) console.error(err);
                    packageSize += data.ContentLength;
                    fs.writeFile(
                      `${outputDir}/${file.url}`,
                      data.Body,
                      (err) => {
                        if (err) throw err;
                        itemsProcessed++;
                        sharedState.progressPercent = Math.round(
                          (itemsProcessed / media.length) * 100
                        );
                        if (itemsProcessed === media.length) {
                          closePackage({
                            ...manifest,
                            size: packageSize,
                            count: media.length,
                            list: media
                          });
                        }
                      }
                    );
                  }
                );
              }
            }
          });
        }
      });
    })
    .catch((err) => console.log(err));
};

const rewriteManifest = (uuid, data, cb) => {
  s3.putObject(
    {
      Bucket: process.env.AWS_BUCKET,
      Key: globalManifest,
      Body: JSON.stringify(data)
    },
    (err) => {
      if (err) {
        console.error(err);
      }
      cloudflare.purge([
        `https://${process.env.AWS_BUCKET}/${process.env.APP_ID}/${process.env.APP_PACKAGE_FOLDER}/manifest.json`,
        `https://${process.env.AWS_BUCKET}/${process.env.APP_ID}/${process.env.APP_PACKAGE_FOLDER}/${uuid}.zip`
      ]);
      if (typeof cb === 'function') cb();
    }
  );
};

const inProgress = (inProgress, uuid) => {
  if (inProgress) {
    sharedState.isInProgress = true;
    sharedState.progressPercent = 0;
    sharedState.uuid = uuid;
    sharedState.package_date = Date.now();
  } else {
    sharedState.isInProgress = false;
    sharedState.progressPercent = 0;
    sharedState.uuid = '';
    sharedState.package_date = '';
    module.exports.updateLibStats();
  }
};

module.exports = {
  secureGET: (req, res) => {
    switch (req.params.action) {
      case 'create':
        if (sharedState.isInProgress) {
          res.writeHead(202, { 'Content-Type': 'text/json' });
          res.write(JSON.stringify({ package: sharedState }));
          res.end();
        } else {
          createPackage(req.params.uuid);
          res.writeHead(200, { 'Content-Type': 'text/json' });
          res.write(JSON.stringify({ package: sharedState }));
          res.end();
        }
        break;
      case 'delete':
        if (sharedState.isInProgress) {
          res.writeHead(202, { 'Content-Type': 'text/json' });
          res.write(JSON.stringify({ package: sharedState }));
          res.end();
        } else {
          deletePackage(req.params.uuid);
          res.writeHead(200, { 'Content-Type': 'text/json' });
          res.write(JSON.stringify({ package: sharedState }));
          res.end();
        }
        break;
      default:
        res.sendStatus(401);
    }
  },

  anonGET: (req, res) => {
    switch (req.params.action) {
      case 'manifest':
        s3Helper.getJSONFrom(
          process.env.AWS_BUCKET,
          globalManifest,
          (manifest) => {
            res.write(
              JSON.stringify({
                url: `https://${process.env.AWS_BUCKET}/${globalManifest}`,
                manifest
              })
            );
            res.end();
          }
        );
        break;
      case 'status':
        module.exports.updateLibStats();
        res.write(
          JSON.stringify({
            status: sharedState,
            appid: process.env.APP_ID,
            bucket: process.env.AWS_BUCKET,
            endpoint: process.env.APP_PACKAGE_FOLDER
          })
        );
        res.end();
        break;
      case 'updatestatus':
        module.exports.updateLibStats();
        res.sendStatus(200);
        break;
      default:
        res.sendStatus(401);
    }
  },

  updateLibStats: () => {
    s3Helper.getFolderSize(
      process.env.AWS_BUCKET,
      `${process.env.APP_ID}/`,
      (library) => {
        sharedState.library = library;
      }
    );
  }
};
