const uuid = require('uuid');
const path = require('path');
const AWS = require('aws-sdk');
AWS.config.region = process.env.AWS_REGION || 'us-east-1';

const s3 = new AWS.S3({
  endpoint: new AWS.Endpoint(process.env.AWS_ENDPOINT)
});

module.exports = {
  signS3: (req, res) => {
    const fileName_original = req.query['file-name'];
    const fileName = `${uuid()}${path.extname(fileName_original)}`;
    const fileType = req.query['file-type'];
    let key =
      process.env.APP_ID.trim().length > 0
        ? `${process.env.APP_ID}/${fileName}`
        : fileName;
    const s3Params = {
      Bucket: process.env.AWS_BUCKET,
      Key: key,
      Expires: 60,
      ContentType: fileType
    };

    s3.getSignedUrl('putObject', s3Params, (err, data) => {
      if (err) {
        console.log(err);
        return res.end();
      }
      const returnData = {
        signedRequest: data,
        originalFilename: fileName_original,
        app_id: process.env.APP_ID,
        filename: fileName,
        uploader: {
          email: req.token.email,
          username: req.token.name,
          role: req.token.role
        }
      };
      res.write(JSON.stringify(returnData));
      res.end();
    });
  },

  delete: (req, res) => {
    let params = {
      Bucket: process.env.AWS_BUCKET,
      Delete: {
        Objects: [{ Key: `${process.env.APP_ID}/${req.body.key}` }]
      }
    };
    s3.deleteObjects(params, function(err) {
      if (err) {
        res.send({ error: err });
        console.log(err, err.stack);
      } else {
        res.send({ deleted: `${process.env.APP_ID}/${req.body.key}` });
      }
    });
  },

  delete_id: (req, res) => {
    let params = {
      Bucket: process.env.AWS_BUCKET,
      Delete: {
        Objects: [{ Key: `${process.env.APP_ID}/${req.params.id}` }]
      }
    };

    s3.deleteObjects(params, function(err) {
      if (err) {
        res.send({ error: err });
        console.log(err, err.stack);
      } else {
        res.send({ deleted: `${req.params.id}` });
      }
    });
  }
};
