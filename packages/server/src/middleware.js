let jwt = require('jsonwebtoken');

module.exports.verifyToken = function (req, res, next) {
  let bearerHeader = req.headers['authorization'];
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    jwt.verify(
      bearerToken,
      process.env.JWT_SECRET,
      { ignoreExpiration: false },
      (err, decodedToken) => {
        if (err) {
          console.log(err);
          res.sendStatus(401);
        } else {
          req.token = decodedToken;
          next();
        }
      }
    );
  } else {
    res.sendStatus(401);
  }
};

module.exports.verifyWithFallback = function (req, res, onVerify, onFallback) {
  let bearerHeader = req.headers['authorization'];
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    jwt.verify(
      bearerToken,
      process.env.JWT_SECRET,
      { ignoreExpiration: false },
      (err) => {
        if (err) {
          onFallback();
        } else {
          onVerify();
        }
      }
    );
  } else {
    onFallback();
  }
};
