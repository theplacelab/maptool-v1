const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');
const util = require('./util.js');
let jwt = require('jsonwebtoken');
const axios = require('axios');
const generatePassword = require('password-generator');

module.exports = {
  initMailClient: () => {
    let debugmode =
      process.env.DEBUG_MODE &&
      process.env.DEBUG_MODE.trim().toLowerCase() === 'true';

    const client = debugmode
      ? nodemailer.createTransport({
          host: 'smtp.mailtrap.io',
          port: 2525,
          auth: {
            user: process.env.MAILTRAP_USER,
            pass: process.env.MAILTRAP_PASS
          },
          pool: true,
          rateDelta: 10000,
          rateLimit: 1,
          maxConnections: 1
        })
      : nodemailer.createTransport(
          sgTransport({
            auth: {
              api_key: process.env.SENDGRID_API_KEY
            },
            pool: true,
            rateDelta: 10000,
            rateLimit: 1,
            maxConnections: 1
          })
        );
    return client;
  },

  wrapInTemplate: (subject, text) => {
    return `<h2>${subject}</h2>${text}<h4>This is an automated message from ${process.env.MAIL_FROM_NAME}, please do not reply.<br/>If you did not request this email, you can ignore it.</h4>`;
  },

  send: (client, options) => {
    client.sendMail(
      {
        from: process.env.MAIL_FROM_NAME
          ? `${process.env.MAIL_FROM_NAME} ${process.env.MAIL_FROM_EMAIL}`
          : process.env.MAIL_FROM_EMAIL,
        ...options,
        text: options.message,
        html: module.exports.wrapInTemplate(options.subject, options.message)
      },
      function(err, info) {
        let debugmode =
          process.env.DEBUG_MODE &&
          process.env.DEBUG_MODE.trim().toLowerCase() === 'true';
        if (err) {
          console.error(err);
        } else if (debugmode) {
          console.log('Message sent: ' + info.response);
        }
      }
    );
  },

  send_test_email: (req, res, opt) => {
    let reqToken;
    if (req.headers['authorization']) {
      const bearer = req.headers['authorization'].split(' ');
      reqToken = bearer[1];
    }
    util.decodeToken(
      reqToken,
      token => {
        console.log(token);
        module.exports.send(opt.client, {
          to: token.decoded.email,
          subject: `Test email to ${token.decoded.email}`,
          message: `Hello ${token.decoded.name} this is a test!`
        });
        res.sendStatus(200);
        return;
      },
      () => res.sendStatus(401)
    );
  },

  request_validation: (req, res, opt) => {
    let reqToken;
    if (req.headers['authorization']) {
      const bearer = req.headers['authorization'].split(' ');
      reqToken = bearer[1];
    }
    util.decodeToken(
      reqToken,
      requestingToken => {
        let validateUser;
        const userID = opt.user_id ? opt.user_id : requestingToken.id;
        const apiURL = `${process.env.REACT_APP_BASE_URL}/api/users?id=eq.${userID}`;
        axios
          .get(apiURL, {
            headers: {
              Authorization: 'Bearer ' + requestingToken.encoded,
              'Content-Type': 'application/json; charset=utf-8'
            }
          })
          .then(response => {
            if (response.data.length === 0) {
              res.writeHead(200, { 'Content-Type': 'text/html' });
              res.write(`<h1>No such user id: ${userID}</h2>`);
              res.end();
            } else {
              validateUser = response.data[0];
              const allowValidation =
                requestingToken.decoded.id === validateUser.id ||
                requestingToken.decoded.group_role === 'websuper';
              if (validateUser.is_validated) {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(
                  `<h1>Email ${validateUser.email} is already validated!</h2>`
                );
                res.end();
              } else if (!allowValidation) {
                res.sendStatus(401);
              } else {
                const validationToken = jwt.sign(
                  {
                    ...validateUser,
                    exp: Math.floor(Date.now() / 1000) + 5 * 60
                  },
                  `${process.env.JWT_SECRET}`
                );
                let validationLink = `${process.env.REACT_APP_BASE_URL}/server/user/validate/${validationToken}`;
                module.exports.send(opt.client, {
                  to: validateUser.email,
                  subject: `${
                    validateUser.name ? `Hello ${validateUser.name}!` : ''
                  } - Please Validate Your Email`,
                  message: `Please validate your email by <a href="${validationLink}">clicking this link</a> or copying the following into your browser:  <br/><br/> <tt>${validationLink}</tt>`
                });
                res.sendStatus(200);
              }
            }
          })
          .catch(err => {
            console.log(err);
            res.sendStatus(401);
          });
      },
      () => res.sendStatus(401)
    );
  },

  resetPassword: (req, res, opt) => {
    let debugmode =
      process.env.DEBUG_MODE &&
      process.env.DEBUG_MODE.trim().toLowerCase() === 'true';

    let userToUpdate;
    const apiURL = isNaN(opt.user_id)
      ? `${process.env.REACT_APP_BASE_URL}/api/users?email=eq.${opt.user_id}`
      : `${process.env.REACT_APP_BASE_URL}/api/users?id=eq.${opt.user_id}`;
    const authorizationToken = jwt.sign(
      {
        role: 'websuper',
        exp: Math.floor(Date.now() / 1000) + 1 * 60
      },
      `${process.env.JWT_SECRET}`
    );
    axios
      .get(apiURL, {
        headers: {
          Authorization: 'Bearer ' + authorizationToken,
          'Content-Type': 'application/json; charset=utf-8'
        }
      })
      .then(response => {
        if (response.data.length === 0) {
          res.writeHead(200, { 'Content-Type': 'text/html' });
          if (debugmode) res.write(`<h1>No such user id: ${opt.user_id}</h2>`);
          res.end();
        } else {
          userToUpdate = response.data[0];

          const newpass = generatePassword();
          module.exports.updateUser(res, {
            id: userToUpdate.id,
            pass: newpass,
            force_newpass: true
          });

          module.exports.send(opt.client, {
            to: userToUpdate.email,
            subject: `Your password has been changed`,
            message: `${
              userToUpdate.name ? `Hello ${userToUpdate.name}!` : ''
            } - Your new temporary password is <b>${newpass}</b>. You will be asked to change it the next time you log in.`
          });
          res.sendStatus(200);
        }
      })
      .catch(err => {
        console.log(err);
        res.sendStatus(401);
      });
  },

  updateUser: (res, data) => {
    const apiURL = `${process.env.REACT_APP_BASE_URL}/api/users?id=eq.${data.id}`;
    const authorizationToken = jwt.sign(
      {
        role: 'websuper',
        exp: Math.floor(Date.now() / 1000) + 1 * 60
      },
      `${process.env.JWT_SECRET}`
    );
    axios
      .patch(apiURL, data, {
        headers: {
          Authorization: 'Bearer ' + authorizationToken,
          'Content-Type': 'application/json; charset=utf-8'
        }
      })
      .then(response => {
        if (response.status !== 204) {
          res.sendStatus(response.status);
        }
      })
      .catch(err => {
        console.log(err);
        res.sendStatus(401);
      });
  },

  validate: (req, res, opt) => {
    util.decodeToken(
      opt.validation_token,
      token => {
        module.exports.updateUser(res, {
          id: token.decoded.id,
          is_validated: true
        });
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(
          `<h1>${process.env.MAIL_FROM_NAME}</h1> <h2>${token.decoded.email} now validated!</h2>`
        );
        res.end();
      },
      () => {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(
          `<h1>${process.env.MAIL_FROM_NAME}</h1> <h2>FAILED: Validation link invalid or expired.</h2>`
        );
        res.end();
      }
    );
  }
};
