const fs = require('fs-extra');
const path = require('path');
let jwt = require('jsonwebtoken');

module.exports = {
  environmentOK: () => {
    let fail =
      !process.env.AWS_ACCESS_KEY_ID ||
      !process.env.AWS_SECRET_ACCESS_KEY ||
      !process.env.AWS_BUCKET ||
      !process.env.AWS_ENDPOINT ||
      !process.env.APP_ID ||
      !process.env.JWT_SECRET;

    let mailfail =
      !process.env.MAILTRAP_USER ||
      !process.env.MAILTRAP_PASS ||
      !process.env.SENDGRID_API_KEY;

    if (mailfail)
      module.exports.log('WARNING: Email config variables not set!');

    if (fail) module.exports.log('FATAL: Environment variables not set!');
    return !fail;
  },

  log: message => {
    if (
      process.env.DEBUG_MODE &&
      process.env.DEBUG_MODE.trim().toLowerCase() === 'true'
    )
      console.log(message);
  },

  saveJSONToFile: (json, directory, file) => {
    if (typeof json === 'undefined') {
      console.error(`Cannot save ${file} to package, data missing in post.`);
      return;
    }
    let outputFile = fs.createWriteStream(path.join(directory, file), {
      flags: 'w'
    });
    outputFile.write(JSON.stringify(json), () => {
      outputFile.end();
    });
  },

  decodeToken: (reqOrToken, onSuccess, onFail) => {
    let token;
    if (reqOrToken.headers) {
      let bearerHeader = reqOrToken.headers['authorization'];
      if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        token = bearer[1];
      }
    } else {
      token = reqOrToken;
    }
    jwt.verify(
      token,
      process.env.JWT_SECRET,
      { ignoreExpiration: false },
      (err, decodedToken) => {
        if (err) {
          if (typeof onFail === 'function') {
            onFail(err);
          } else {
            console.error(err);
          }
        } else {
          onSuccess({ encoded: token, decoded: decodedToken });
        }
      }
    );
  }
};
