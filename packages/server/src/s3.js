const AWS = require('aws-sdk');
AWS.config.region = process.env.AWS_REGION || 'us-east-1';
const s3 = new AWS.S3({
  endpoint: new AWS.Endpoint(process.env.AWS_ENDPOINT)
});
module.exports = {
  getFolderSize: (Bucket, Prefix, cb) => {
    s3.listObjects(
      {
        Bucket,
        Prefix
      },
      (err, objects) => {
        if (err) {
          console.error(err);
          cb({ size: 0, count: 0 });
        } else {
          let filesize = 0;
          objects.Contents.forEach(file => (filesize += file.Size));
          cb({ size: filesize, count: objects.Contents.length });
        }
      }
    );
  },

  getFileSize: (Bucket, Key, cb) => {
    s3.headObject(
      {
        Key,
        Bucket
      },
      (err, stats) => {
        if (err) {
          console.error(err);
          cb({});
        } else {
          cb(stats);
        }
      }
    );
  },

  getJSONFrom: (Bucket, Key, cb) => {
    s3.getObject(
      {
        Key,
        Bucket
      },
      (err, data) => {
        if (err) {
          console.error(err);
          cb([]);
        } else {
          try {
            cb(JSON.parse(data.Body.toString('utf-8')));
          } catch (e) {
            cb([]);
          }
        }
      }
    );
  },

  deleteFiles: fileArray => {
    let Objects = [];
    fileArray.forEach(file => Objects.push({ Key: file }));
    s3.deleteObjects(
      {
        Bucket: process.env.AWS_BUCKET,
        Delete: {
          Objects
        }
      },
      function(err) {
        if (err) {
          console.log(err, err.stack);
        }
      }
    );
  }
};
