DROP VIEW api.s_selection_options;
CREATE OR REPLACE VIEW api.s_selection_options AS
SELECT DISTINCT poi_data.value,
    poi_datatype.id AS data_type_id,
    map_id
FROM private.poi_datatype
    INNER JOIN private.poi_data ON private.poi_data.data_type_id = private.poi_datatype.id
WHERE poi_datatype.format_id = 2
-- SELECTION
ORDER BY data_type_id,
         value;

--- Permissions
GRANT
    SELECT ON api.s_selection_options TO webuser;
