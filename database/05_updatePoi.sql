DROP FUNCTION IF EXISTS api.update_poidata
(json);
CREATE OR REPLACE FUNCTION api.update_poidata
(payload json) returns JSON AS
$$
DECLARE
    RETVAL JSON ;
DECLARE item JSON;
BEGIN
    UPDATE private.poi
    SET name=(payload ->> 'name')
    ::text,
        map_id=
    (payload ->> 'map_id')::int,
        date=
    (payload ->> 'date')::text,
        coordinate=
    (payload ->> 'coordinate')::json,
        representation_id =
    (payload ->> 'representation_id')::int,
        representation_config=
    (payload ->> 'representation_config')::json,
        presentation_id =
    (payload ->> 'presentation_id')::int,
        presentation_config=
    (payload ->> 'presentation_config')::json
    WHERE id =
    (payload ->> 'id')::int;
IF FOUND THEN
        FOR item IN
SELECT *
FROM Json_array_elements((payload ->> 'data')
::json)
            LOOP
INSERT INTO private.poi_data
    (poi_id, data_type_id, "value", is_published, is_locked, is_markdown)
SELECT (payload ->> 'id'
)::int,
(item ->> 'data_type_id')::int,
(item ->> 'value')::text,
(item ->> 'is_published')::boolean,
(item ->> 'is_locked')::boolean,
(item ->> 'is_markdown')::boolean
                ON CONFLICT ON CONSTRAINT unique_poi_datatype_pair DO
UPDATE SET "value"     = (item ->> 'value')
::text,
                                                                                 is_published=
(item ->> 'is_published')::boolean,
                                                                                 is_locked=
(item ->> 'is_locked')::boolean,
                                                                                 is_markdown =
(item ->> 'is_markdown')::boolean;
END LOOP;
SELECT json_agg(t)
FROM (SELECT *
    from private.poi_data
    WHERE poi_id = (payload ->> 'id')::int
) t INTO RETVAL;
RETURN RETVAL;
ELSE
RETURN -1;
END
IF;
END;
$$ LANGUAGE plpgsql SECURITY DEFINER
-- Set a secure search_path: trusted schema(s), then 'pg_temp'.
SET search_path
= admin, pg_temp;
REVOKE ALL PRIVILEGES ON FUNCTION api.update_poidata
(payload json) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION api.update_poidata
(payload json) TO webuser;