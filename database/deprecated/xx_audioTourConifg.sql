DROP VIEW IF EXISTS api.audiotourconfig;
CREATE OR REPLACE VIEW api.audiotourconfig AS
SELECT audiotour.id         AS audiotour_id,
    audiotour.trigger_type_id,
    audiotour.track_id,
    audiotour.sortorder,
    audiotour.poi_id,
    audiotour.does_loop,
    audiotour.does_duck,
    audiotour.only_once,
    audiotour.never_after,
    audiotour.only_after,
    audiotour.follow_at,
    audiotour.displayname,
    poi.sortorder        as poi_sortorder,
    poi.name             as poi_name,
    poi.coordinate,
    media.url            as media_url,
    media.name           as media_name,
    media.type           as media_type,
    media.preview        as media_preview,
    transition_type.name AS transition,
    transition_type.id   AS transition_id,
    trigger_type.name    AS trigger,
    map.id               as map_id,
    map.uuid             as map_uuid,
    map.title             as map_title
FROM private.audiotour
    LEFT JOIN private.media on media.id = audiotour.media_id
    LEFT JOIN private.poi on poi.id = audiotour.poi_id
    LEFT JOIN private.transition_type on transition_type.id = audiotour.transition_type_id
    LEFT JOIN private.trigger_type on trigger_type.id = audiotour.trigger_type_id
    LEFT JOIN private.map on map.id = audiotour.map_id
ORDER BY transition_type_id, track_id, sortorder, poi_sortorder;