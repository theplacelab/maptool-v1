DROP VIEW IF EXISTS api.const;

CREATE OR REPLACE VIEW api.const AS
SELECT (SELECT array_to_json(array_agg(ts.*)) AS array_to_json
        FROM (SELECT const as string, id as id
              FROM private.transition_type) ts)    as transition,
       (SELECT array_to_json(array_agg(ts.*)) AS array_to_json
        FROM (SELECT const as string, id as id
              FROM private.trigger_type) ts)       as "trigger",
       (SELECT array_to_json(array_agg(ts.*)) AS array_to_json
        FROM (SELECT const as string, id as id
              FROM private.poi_representation) ts) as "representation",
       (SELECT '[{"string":"ANONYMOUS","id":"webanon"},{"string":"USER","id":"webuser"},{"string":"SUPERUSER","id":"websuper"}]'::jsonb)
           AS role;

GRANT
    SELECT ON api.const TO webanon;