-- Used exclusively by server on server side

DROP VIEW IF EXISTS api.s_media_manifest;
CREATE OR REPLACE VIEW api.s_media_manifest AS
SELECT map.id AS map_id, media.*
from private.media
         INNER JOIN private.poi_trigger_config ON poi_trigger_config.media_id = media.id
         INNER JOIN private.poi_trigger ON poi_trigger.id = poi_trigger_config.poitrigger_id
         INNER JOIN private.poi ON poi_trigger.poi_id = poi.id
         INNER JOIN private.map ON map.id = poi.map_id
WHERE map.id = 1
ORDER BY name;

GRANT
    SELECT,
    UPDATE ON api.s_media_manifest TO webuser;

GRANT
    INSERT,
    DELETE ON api.s_media_manifest TO websuper;
