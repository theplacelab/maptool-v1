DROP VIEW IF EXISTS api.mediaPerMap;
CREATE OR REPLACE VIEW api.mediaPerMap AS
SELECT DISTINCT(media_id) as media_id, media.*, map.id as map_id, map.uuid as map_uuid
FROM private.audiotour
         INNER JOIN private.media ON audiotour.media_id = media.id
         INNER JOIN private.map ON map.id = audiotour.map_id;

GRANT
    SELECT ON api.mediaPerMap TO webanon;
