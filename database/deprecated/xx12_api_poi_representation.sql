DROP VIEW IF EXISTS api.s_poi_representation;

CREATE OR REPLACE VIEW api.s_poi_representation AS
SELECT poi_representation.*
FROM private.poi_representation;


CREATE OR REPLACE RULE poi_rep_upd AS ON
    UPDATE TO api.s_poi_representation DO INSTEAD
    UPDATE private.poi_representation
    SET name          = NEW.name,
        template      = NEW.template,
        is_selectable = NEW.is_selectable
    WHERE id = NEW.id
    RETURNING poi_representation.*;


CREATE OR REPLACE RULE poi_rep_ins AS ON
    INSERT TO api.s_poi_representation DO INSTEAD
    INSERT INTO private.poi_representation (name, template, is_selectable)
    VALUES (NEW.name,
            NEW.template,
            NEW.is_selectable)
    RETURNING poi_representation.*;


CREATE OR REPLACE RULE poi_rep_del AS ON
    DELETE TO api.s_poi_representation DO INSTEAD
    DELETE
    FROM private.poi_representation
    WHERE id = OLD.id
    RETURNING poi_representation.*;


--- Permissions
GRANT
    SELECT ON api.s_poi_representation TO webanon;

GRANT
    INSERT,
    DELETE,
    UPDATE ON api.s_poi_representation TO webuser;
