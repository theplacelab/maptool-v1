CREATE TABLE private.audiotour (
    id integer NOT NULL,
    track_id integer,
    transition_type_id integer,
    transition_options jsonb,
    media_id integer,
    does_loop boolean DEFAULT false,
    does_duck boolean DEFAULT false,
    sortorder integer DEFAULT 0,
    trigger_type_id integer,
    map_id integer,
    poi_id integer,
    only_once boolean DEFAULT false,
    never_after integer,
    only_after integer,
    follow_at text,
    trigger_type_options jsonb,
    proximity_id integer,
    on_weather integer,
    on_tod integer,
    fires_on_count text,
    fires_on_count_global text,
    session_skip boolean DEFAULT false,
    displayname text
);
CREATE TABLE private.map (
    id integer NOT NULL,
    title text,
    is_published boolean DEFAULT true,
    preferences json,
    tileserver_id integer,
    uuid text
);
CREATE TABLE private.audiotour_config (
    map_id integer NOT NULL,
    metadata jsonb DEFAULT '{}'::jsonb,
    weather jsonb,
    bellwether_poi integer,
    ideal_path jsonb,
    ideal_path_threshold double precision DEFAULT '20'::double precision
);
CREATE TABLE private.poi (
    id SERIAL,
    name text,
    map_id integer NOT NULL,
    representation_id integer DEFAULT 1 NOT NULL,
    coordinate json DEFAULT '{"lat":0,"lng":0}'::json,
    date text,
    representation_config json,
    sortorder integer,
    presentation_config json,
    presentation_id integer DEFAULT 1
);
CREATE TABLE private.poi_datatype (
    id integer NOT NULL,
    name text,
    is_published_by_default boolean DEFAULT true,
    format_id integer DEFAULT 1,
    display_order integer,
    map_id integer,
    search_scope text,
    appears_in_search_results text DEFAULT false,
    is_locked_by_default boolean DEFAULT false,
    is_markdown_by_default boolean DEFAULT false
);
CREATE TABLE private.tileservers (
    id integer NOT NULL,
    url text,
    attribution text,
    notes text,
    slug text,
    properties json
);
CREATE TABLE private.media (
    id SERIAL,
    name text,
    url text,
    preview text,
    type text,
    exif jsonb,
    preview_exif jsonb,
    is360 boolean DEFAULT false
);
CREATE TABLE private.poi_data (
    id SERIAL,
    data_type_id integer,
    value text,
    poi_id integer,
    is_published boolean DEFAULT true,
    is_locked boolean DEFAULT false,
    is_markdown boolean DEFAULT false
);
CREATE TABLE private.proximity (
    id integer NOT NULL,
    name text,
    options jsonb DEFAULT '{}'::jsonb,
    map_id integer,
    sortorder integer
);
CREATE TABLE api.users (
    email text NOT NULL,
    pass text NOT NULL,
    group_role name NOT NULL,
    id integer NOT NULL,
    name text,
    is_validated boolean DEFAULT false,
    force_newpass boolean DEFAULT false,
    picture text DEFAULT 'https://assets.theplacelab.com/demo/user.png'::text,
    CONSTRAINT users_email_check CHECK ((email ~* '^.+@.+\..+$'::text)),
    CONSTRAINT users_pass_check CHECK ((length(pass) < 512)),
    CONSTRAINT users_role_check CHECK ((length((group_role)::text) < 512))
);
CREATE TABLE private.track (
    id integer NOT NULL,
    name text,
    description text,
    map_id integer
);
CREATE TABLE private.transition (
    id SERIAL,
    name text,
    options_template json,
    "mediaRequired" boolean
);
