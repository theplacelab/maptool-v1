--{"payload":{
--  "data": [
--      {"poi_id":1335, track_id:0, trigger_type_id:3, "sortorder":0},
--		{"poi_id":1333, track_id:0, trigger_type_id:3, "sortorder":1},
--		{"poi_id":1332, track_id:0, trigger_type_id:3, "sortorder":2},
--		{"poi_id":1330, track_id:0, trigger_type_id:3, "sortorder":3} 
--  ]
--}}
DROP FUNCTION IF EXISTS api.resequence_track_items
    (json);
CREATE OR REPLACE FUNCTION api.resequence_track_items(payload json)
    returns JSON AS
$$
DECLARE
    DECLARE
    item JSON;
BEGIN

    FOR item IN
        SELECT *
        FROM Json_array_elements((payload ->> 'data')
            ::json)
        LOOP
            UPDATE private.audiotour
            SET sortorder =
                    (item ->> 'sortorder')
                        ::int
            WHERE id =
                  (item ->> 'audiotour_id')::int;
        END LOOP;
    RETURN 1;

END;
$$ LANGUAGE plpgsql
    SECURITY DEFINER
-- Set a secure search_path: trusted schema(s), then 'pg_temp'.
    SET search_path
        = admin, pg_temp;


REVOKE ALL PRIVILEGES ON FUNCTION api.resequence_track_items
    (payload json) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION api.resequence_track_items
    (payload json) TO webuser;
