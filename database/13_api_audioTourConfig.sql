DROP VIEW IF EXISTS api.audiotour_config;
CREATE OR REPLACE VIEW api.audiotour_config AS
SELECT audiotour_config.*,
    map.uuid as map_uuid
FROM private.audiotour_config
    INNER JOIN private.map ON map.id = audiotour_config.map_id;


CREATE OR REPLACE RULE audiotour_config_upd AS ON
UPDATE TO api.audiotour_config DO INSTEAD
UPDATE private.audiotour_config
    SET metadata = NEW.metadata,
        ideal_path = NEW.ideal_path,
        ideal_path_threshold = NEW.ideal_path_threshold,
        weather = NEW.weather,
        bellwether_poi= NEW.bellwether_poi
    WHERE map_id = NEW.map_id
RETURNING NEW.*;


CREATE OR REPLACE RULE audiotour_config_ins AS ON
INSERT TO
api.audiotour_config
DO
INSTEAD
INSERT INTO private.audiotour_config
    (map_id,metadata,ideal_path,ideal_path_threshold,weather,bellwether_poi)
VALUES
    (NEW.map_id,
        NEW.metadata,
        NEW.ideal_path,
        NEW.ideal_path_threshold,
        NEW.weather,
        NEW.bellwether_poi);


CREATE OR REPLACE RULE audiotour_config_del AS ON
DELETE TO api.audiotour_config DO
INSTEAD
DELETE
    FROM private.audiotour_config
    WHERE map_id = OLD.map_id;

--- Permissions
GRANT
    SELECT
    ON
    api.audiotour_config TO webanon;
GRANT
    SELECT,
    INSERT,
    DELETE,
    UPDATE ON
    api.audiotour_config TO webuser;
