ALTER TABLE private.poi_data
    DROP CONSTRAINT IF EXISTS unique_poi_datatype_pair
CASCADE;

ALTER TABLE private.poi_data
    ADD CONSTRAINT unique_poi_datatype_pair UNIQUE (poi_id, data_type_id);


CREATE OR REPLACE VIEW api.s_poi AS
SELECT poi.*
FROM private.poi
    INNER JOIN private.map ON map.id = poi.map_id;


CREATE OR REPLACE RULE poi_upd AS ON
UPDATE TO api.s_poi DO INSTEAD
UPDATE private.poi
    SET name                  = NEW.name,
        representation_id     = NEW.representation_id,
        representation_config = NEW.representation_config,
        presentation_id       = NEW.presentation_id,
        presentation_config   = NEW.presentation_config,
        coordinate            = NEW.coordinate,
        sortorder             = NEW.sortorder,
        "date"                = NEW.date
    WHERE id = NEW.id
RETURNING poi.*;


CREATE OR REPLACE RULE poi_ins AS ON
INSERT TO
api.s_poi
DO
INSTEAD
INSERT INTO private.poi
    (name, map_id, representation_id, representation_config, presentation_id, presentation_config, coordinate, sortorder, date)
VALUES
    (NEW.name,
        NEW.map_id,
        NEW.representation_id,
        NEW.representation_config,
        NEW.presentation_id,
        NEW.presentation_config,
        NEW.coordinate,
        NEW.sortorder,
        NEW.date)
RETURNING poi.*;


CREATE OR REPLACE RULE poi_del AS ON
DELETE TO api.s_poi DO
INSTEAD
DELETE
    FROM private.poi
    WHERE id = OLD.id
RETURNING poi.*;


DROP VIEW IF EXISTS api.poi_with_data;
CREATE OR REPLACE VIEW api.poi_with_data AS
SELECT map.id                            AS map_id,
    map.uuid                          AS map_uuid,
    private.poi.id                    AS poi_id,
    private.poi.representation_id     AS representation_id,
    private.poi.representation_config AS representation_config,
    private.poi.presentation_id       AS presentation_id,
    private.poi.presentation_config   AS presentation_config,
    poi_data.id                       AS poi_data_id,
    private.poi.name                  AS poi_name,
    private.poi.date                  AS poi_date,
    private.poi.coordinate            AS poi_coordinate,
    private.poi.sortorder             AS poi_sortorder,
    poi_data.is_locked                AS is_locked,
    poi_data.is_published             AS is_public,
    poi_data.is_markdown              AS is_markdown,
    poi_data.data_type_id             AS field_data_type,
    poi_datatype.name                 AS field,
    poi_datatype.search_scope         AS search_scope,
    poi_data.value                    AS field_value,
    poi_datatype.format_id                AS format_id,
    (SELECT array_to_json(array_agg(t.*)) AS OPTIONS
    FROM (SELECT DISTINCT value AS OPTION
        FROM private.poi_data
        WHERE poi_datatype.format_id = 2 --'SELECTION'
            AND poi_id = poi.id
        ORDER BY value ASC) t)
FROM private.poi
    LEFT OUTER JOIN private.poi_data ON (poi_data.poi_id = private.poi.id
        AND poi_data.is_published IS TRUE)
    LEFT OUTER JOIN private.poi_datatype ON poi_data.data_type_id = poi_datatype.id
    INNER JOIN private.map ON map.id = poi.map_id
        AND map.is_published = TRUE
ORDER BY poi_sortorder ASC,
         poi_id,
         poi_data_id;

DROP VIEW IF EXISTS api.s_poi_with_data;
CREATE OR REPLACE VIEW api.s_poi_with_data AS
SELECT map.id                            AS map_id,
    private.poi.id                    AS poi_id,
    private.poi.representation_id     AS representation_id,
    private.poi.representation_config AS representation_config,
    private.poi.presentation_id AS presentation_id,
    private.poi.presentation_config AS presentation_config,
    poi_data.id                       AS poi_data_id,
    private.poi.name                  AS poi_name,
    private.poi.date                  AS poi_date,
    private.poi.coordinate            AS poi_coordinate,
    private.poi.sortorder             AS poi_sortorder,
    poi_data.is_published             AS is_public,
    poi_data.is_locked                AS is_locked,
    poi_data.is_markdown              AS is_markdown,
    poi_data.data_type_id             AS field_data_type,
    poi_datatype.name                 AS field,
    poi_datatype.search_scope         AS search_scope,
    poi_data.value                    AS field_value,
    poi_datatype.format_id                AS format_id,
    (SELECT array_to_json(array_agg(t.*)) AS OPTIONS
    FROM (SELECT DISTINCT value
AS OPTION
        FROM private.poi_data
            INNER JOIN private.poi_datatype ON private.poi_data.data_type_id = private.poi_datatype.id
        WHERE private.poi_datatype.format_id = 2
        ORDER BY value ASC) t)
FROM private.poi
    LEFT OUTER JOIN private.poi_data ON (poi_data.poi_id = private.poi.id)
    LEFT OUTER JOIN private.poi_datatype ON poi_data.data_type_id = poi_datatype.id
    INNER JOIN private.map ON map.id = poi.map_id
        AND map.is_published = TRUE
ORDER BY poi_sortorder ASC,
         poi_id,
         poi_data_id;

DROP VIEW IF EXISTS api.s_poi_data;
CREATE OR REPLACE VIEW api.s_poi_data AS
SELECT *
FROM private.poi_data;

-- FIXME: Should deprecate this endpoint, we want to use only the upsert function below
CREATE RULE s_poidata_ins AS ON
INSERT TO
api.s_poi_data
DO
INSTEAD
INSERT INTO private.poi_data
    (data_type_id, value, poi_id, is_published)
VALUES
    (NEW.data_type_id,
        NEW.value,
        NEW.poi_id,
        NEW.is_published)

RETURNING poi_data.*;

--- Permissions
GRANT
    SELECT ON api.config,
    api.poi_with_data TO webanon;

GRANT
    SELECT,
    INSERT,
    DELETE,
    UPDATE ON
    api.s_poi,
    api.s_poi_with_data,
    api.s_poi_data TO webuser;
