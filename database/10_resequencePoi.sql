--{"payload":{
--  "data": [
--    {"poi_id":1335, "sortorder":0},
--		{"poi_id":1333, "sortorder":1},
--		{"poi_id":1332, "sortorder":2},
--		{"poi_id":1330, "sortorder":3} 
--  ]
--}}
DROP FUNCTION IF EXISTS api.resequence_poi(json);
CREATE OR REPLACE FUNCTION api.resequence_poi(payload json)
    returns JSON AS
$$
DECLARE
    DECLARE
    item JSON;
BEGIN

    FOR item IN
        SELECT *
        FROM Json_array_elements((payload ->> 'data')::json)
        LOOP
            UPDATE private.poi
            SET sortorder =
                    (item ->> 'sortorder')::int
            WHERE id = (item ->> 'poi_id')::int;
        END LOOP;
    RETURN 1;

END;
$$ LANGUAGE plpgsql
    SECURITY DEFINER
    -- Set a secure search_path: trusted schema(s), then 'pg_temp'.
    SET search_path = admin, pg_temp;


REVOKE ALL PRIVILEGES ON FUNCTION api.resequence_poi(payload json) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION api.resequence_poi(payload json) TO webuser;
