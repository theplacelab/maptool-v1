CREATE OR REPLACE FUNCTION api.import(payload text)
    returns INTEGER AS
$$
DECLARE
    newid        INTEGER ;
    DECLARE item JSON;

BEGIN
    INSERT INTO api.s_poi
    (NAME,
     map_id,
     date,
     coordinate,
     representation_id,
     coordinate_id)
    SELECT (payload ->> 'name'):: text,
           (payload ->> 'mapId'):: int,
           (payload ->> 'date'):: text,
           (payload ->> 'coordinate')::json,
           1,
           NULL
    returning id
        INTO newid;

    for item IN
        SELECT *
        FROM Json_array_elements((payload ->> 'data')::json)
        LOOP
            INSERT INTO api.s_poi_data
            (poi_id,
             data_type_id,
             value,
             is_published)
            SELECT newid,
                   (item ->> 'id'):: int,
                   (item ->> 'value')::text,
                   (item ->> 'is_published')::boolean;
        END LOOP;
    RETURN newid;
END;
$$ LANGUAGE plpgsql
    SECURITY DEFINER
    -- Set a secure search_path: trusted schema(s), then 'pg_temp'.
    SET search_path = admin, pg_temp;


REVOKE ALL PRIVILEGES ON FUNCTION api.import(payload text) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION api.import(payload text) TO webuser;
