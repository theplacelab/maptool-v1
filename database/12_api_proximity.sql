CREATE OR REPLACE VIEW api.proximity AS
SELECT proximity.*, map.uuid as map_uuid
FROM private.proximity
         INNER JOIN private.map ON map.id = proximity.map_id;


CREATE OR REPLACE RULE proximity_upd AS ON
UPDATE TO api.proximity DO INSTEAD
UPDATE private.proximity
    SET name      = NEW.name,
        options   = NEW.options,
        sortorder = NEW.sortorder
    WHERE id = NEW.id
RETURNING NEW.*;


CREATE OR REPLACE RULE proximity_ins AS ON
INSERT TO
api.proximity
    DO
    INSTEAD
INSERT INTO private.proximity
    (name, map_id, options, sortorder)
VALUES
    (NEW.name,
        NEW.map_id,
        NEW.options,
        NEW.sortorder)
RETURNING proximity.*, NULL as map_id;

CREATE OR REPLACE RULE proximity_del AS ON
DELETE TO api.proximity DO
INSTEAD
DELETE
    FROM private.proximity
    WHERE id = OLD.id
RETURNING proximity.*, NULL as map_id;

--- Permissions
GRANT
    SELECT
    ON
    api.proximity TO webanon;
GRANT
    SELECT,
    INSERT,
    DELETE,
    UPDATE ON
    api.proximity TO webuser;

-------------

DROP FUNCTION IF EXISTS api.resequence_proximity
(json);
CREATE OR REPLACE FUNCTION api.resequence_proximity
(payload json)
    returns JSON AS
$$
DECLARE
DECLARE
    item JSON;
BEGIN

    FOR item IN
    SELECT *
    FROM Json_array_elements((payload ->> 'data')
    ::json)
        LOOP
    UPDATE private.proximity
            SET sortorder =
                    (item ->> 'sortorder')
    ::int
            WHERE id =
    (item ->> 'id')::int;
END LOOP;
RETURN 1;

END;
$$ LANGUAGE plpgsql
    SECURITY DEFINER
-- Set a secure search_path: trusted schema(s), then 'pg_temp'.
SET search_path
= admin, pg_temp;


REVOKE ALL PRIVILEGES ON FUNCTION api.resequence_proximity
(payload json) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION api.resequence_proximity
(payload json) TO webuser;
