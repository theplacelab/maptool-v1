DROP FUNCTION IF EXISTS api.update_audiotouritem
(json);
CREATE OR REPLACE FUNCTION api.update_audiotouritem
(payload json) returns JSON AS
$$
DECLARE
    RETVAL JSON ;
DECLARE item JSON;
BEGIN
    UPDATE private.audiotour
    SET track_id=(payload ->> 'track_id')
    ::int,
        transition_type_id=
    (payload ->> 'transition_type_id')::int,
        media_id=
    (payload ->> 'media_id')::int,
        transition_options=
    (payload ->> 'transition_options')::json,
        does_loop=
    (payload ->> 'does_loop')::boolean,
        does_duck=
    (payload ->> 'does_duck')::boolean,
        on_weather=
    (payload ->> 'on_weather')::int,
        on_tod=
    (payload ->> 'on_tod')::int,
        fires_on_count=
    (payload ->> 'fires_on_count')::text,
        fires_on_count_global =
    (payload ->> 'fires_on_count_global')::text,
        sortorder=
    (payload ->> 'sortorder')::int,
        trigger_type_id=
    (payload ->> 'trigger_type_id')::int,
        map_id=
    (payload ->> 'map_id')::int,
        poi_id=
    (payload ->> 'poi_id')::int,
        only_once=
    (payload ->> 'only_once')::boolean,
        session_skip=
    (payload ->> 'session_skip')::boolean,
        never_after=
    (payload ->> 'never_after')::int,
        only_after=
    (payload ->> 'only_after')::int,
        follow_at=
    (payload ->> 'follow_at')::text,
        displayname=
    (payload ->> 'displayname')::text
        
    WHERE id =
    (payload ->> 'id')::int;
IF FOUND THEN
SELECT json_agg(t)
FROM (SELECT *
    from private.audiotour
    WHERE id = (payload ->> 'id')::int
) t INTO RETVAL;
RETURN RETVAL;
ELSE
RETURN -1;
END
IF;
END;
$$ LANGUAGE plpgsql SECURITY DEFINER
SET search_path
= admin, pg_temp;
REVOKE ALL PRIVILEGES ON FUNCTION api.update_audiotouritem
(payload json) FROM PUBLIC;
GRANT EXECUTE ON FUNCTION api.update_audiotouritem
(payload json) TO webuser;