DROP VIEW IF EXISTS api.config;


CREATE OR REPLACE VIEW api.config AS
SELECT map.id,
       map.title,
       map.is_published,
       map.preferences,
       map.tileserver_id,
       map.uuid,
       tileservers.url AS tileserver_url,
       tileservers.properties,
       tileservers.attribution AS tileserver_attribution,

        (SELECT min(poi.date) AS min
         FROM private.poi
         WHERE poi.map_id = map.id) AS date_min,

        (SELECT max(poi.date) AS max
         FROM private.poi
         WHERE poi.map_id = map.id) AS date_max,

        (SELECT array_to_json(array_agg(ts.*)) AS array_to_json
         FROM
                 (SELECT tileservers_1.id,
                         tileservers_1.url,
                         tileservers_1.attribution,
                         tileservers_1.notes,
                         tileservers_1.slug,
                         tileservers_1.properties
                  FROM private.tileservers tileservers_1) ts) AS available_tileservers,

        (SELECT array_to_json(array_agg(ts.*)) AS array_to_json
         FROM
                 (SELECT map_1.id,
                         map_1.title,
                         map_1.uuid
                  FROM private.map map_1
                  ORDER BY map_1.id) ts) AS available_maps,

        (SELECT array_to_json(array_agg(t.*)) AS array_to_json
         FROM
                 (SELECT poi_datatype.id,
                         poi_datatype.name,
                         poi_datatype.is_published_by_default,
                         poi_datatype.format_id,
                         poi_datatype.display_order,
                         poi_datatype.map_id,
                         poi_datatype.search_scope,
                         poi_datatype.appears_in_search_results,
                         poi_datatype.is_locked_by_default,
                         poi_datatype.is_markdown_by_default
                  FROM private.poi_datatype
                  WHERE poi_datatype.map_id = map.id
                  ORDER BY poi_datatype.display_order) t) AS poi_fields
FROM private.map
LEFT JOIN private.tileservers ON tileservers.id = map.tileserver_id
WHERE map.is_published = true
ORDER BY map.id;




CREATE OR REPLACE VIEW api.s_config AS
SELECT map.id,
       map.title,
       map.is_published,
       map.preferences,
       map.tileserver_id,
       map.uuid,
       tileservers.url AS tileserver_url,
       tileservers.properties,
       tileservers.attribution AS tileserver_attribution,

        (SELECT min(poi.date) AS min
         FROM private.poi
         WHERE poi.map_id = map.id) AS date_min,

        (SELECT max(poi.date) AS max
         FROM private.poi
         WHERE poi.map_id = map.id) AS date_max,

        (SELECT array_to_json(array_agg(ts.*)) AS array_to_json
         FROM
                 (SELECT tileservers_1.id,
                         tileservers_1.url,
                         tileservers_1.attribution,
                         tileservers_1.notes,
                         tileservers_1.slug,
                         tileservers_1.properties
                  FROM private.tileservers tileservers_1) ts) AS available_tileservers,

        (SELECT array_to_json(array_agg(ts.*)) AS array_to_json
         FROM
                 (SELECT map_1.id,
                         map_1.title,
                         map_1.uuid
                  FROM private.map map_1
                  ORDER BY map_1.id) ts) AS available_maps,

        (SELECT array_to_json(array_agg(t.*)) AS array_to_json
         FROM
                 (SELECT poi_datatype.id,
                         poi_datatype.name,
                         poi_datatype.is_published_by_default,
                         poi_datatype.format_id,
                         poi_datatype.display_order,
                         poi_datatype.map_id,
                         poi_datatype.search_scope,
                         poi_datatype.appears_in_search_results,
                         poi_datatype.is_locked_by_default,
                         poi_datatype.is_markdown_by_default
                  FROM private.poi_datatype
                  WHERE poi_datatype.map_id = map.id
                  ORDER BY poi_datatype.display_order) t) AS poi_fields
FROM private.map
LEFT JOIN private.tileservers ON tileservers.id = map.tileserver_id
ORDER BY map.id;


CREATE OR REPLACE RULE app_upd AS ON
UPDATE TO api.s_config DO INSTEAD
UPDATE private.map
SET title = NEW.title,
    is_published = NEW.is_published,
    preferences = NEW.preferences,
    tileserver_id = NEW.tileserver_id,
    uuid = NEW.uuid
WHERE id = NEW.id RETURNING NEW.*;


CREATE OR REPLACE RULE app_ins AS ON
INSERT TO api.s_config DO INSTEAD
INSERT INTO private.map (title, is_published, preferences, tileserver_id, uuid)
VALUES (NEW.title,
        NEW.is_published,
        NEW.preferences,
        NEW.tileserver_id,
        NEW.uuid);

GRANT
SELECT ON api.config TO webanon;

GRANT
SELECT,
INSERT,
DELETE,
UPDATE ON api.config,
          api.s_config TO webuser;