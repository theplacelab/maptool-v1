DROP VIEW IF EXISTS api.mediaconfig;
CREATE OR REPLACE VIEW api.mediaconfig AS
SELECT map.id                                            as map_id,

    (SELECT array_to_json(array_agg(ts.*)) AS array_to_json
    FROM (SELECT media.*
        FROM private.media) ts)     AS available_media


FROM private.map
WHERE private.map.is_published = TRUE;

DROP VIEW IF EXISTS api.s_mediaconfig;
CREATE OR REPLACE VIEW api.s_mediaconfig AS
SELECT map.id                                            as map_id,

    (SELECT array_to_json(array_agg(ts.*)) AS array_to_json
    FROM (SELECT media.*
        FROM private.media) ts)     AS available_media


FROM private.map;

CREATE OR REPLACE VIEW api.media AS
SELECT media.*
FROM private.media;

CREATE OR REPLACE VIEW api.s_media AS
SELECT media.*
FROM private.media;


CREATE OR REPLACE RULE media_upd AS ON
UPDATE TO api.s_media DO INSTEAD
UPDATE private.media
    SET name         = NEW.name,
        url          = NEW.url,
        preview      = NEW.preview,
        preview_exif = NEW.preview_exif,
        exif         = NEW.exif,
        is360        = NEW.is360,
        type         = NEW.type
    WHERE id = NEW.id
RETURNING *;

CREATE OR REPLACE RULE media_ins AS ON
INSERT TO
api.s_media
DO
INSTEAD
INSERT INTO private.media
    (name, url, exif, preview, preview_exif, is360, type)
VALUES
    (NEW.name,
        NEW.url,
        NEW.exif,
        NEW.preview,
        NEW.preview_exif,
        NEW.is360,
        NEW.type)
RETURNING *;


CREATE OR REPLACE RULE media_del AS ON
DELETE TO api.s_media DO
INSTEAD
DELETE
    FROM private.media
    WHERE id = OLD.id
RETURNING *;

--- Permissions
GRANT
    SELECT ON api.media TO webanon;

GRANT
    SELECT,
    INSERT,
    DELETE,
    UPDATE ON api.s_media TO webuser;

GRANT
    SELECT ON api.mediaconfig TO webanon;

GRANT
    SELECT,
    INSERT,
    DELETE,
    UPDATE ON api.s_mediaconfig TO webuser;
