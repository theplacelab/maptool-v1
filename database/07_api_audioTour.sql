DROP VIEW IF EXISTS api.audiotour;
CREATE OR REPLACE VIEW api.audiotour AS
SELECT audiotour.*, map.uuid as map_uuid
FROM private.audiotour
    INNER JOIN private.map ON map.id = audiotour.map_id;
CREATE OR REPLACE RULE audiotour_upd AS
    ON
UPDATE TO api.audiotour DO INSTEAD
UPDATE private.audiotour
    SET track_id             = NEW.track_id,
        transition_type_id   = NEW.transition_type_id,
        transition_options   = NEW.transition_options,
        displayname=NEW.displayname,
        media_id=NEW.media_id,
        does_loop=NEW.does_loop,
        does_duck=NEW.does_duck,
        on_weather=NEW.on_weather,
        on_tod=NEW.on_tod,
        fires_on_count=NEW.fires_on_count,
        fires_on_count_global=NEW.fires_on_count_global,
        only_once=NEW.only_once,
        session_skip=NEW.session_skip,
        never_after=NEW.never_after,
        only_after=NEW.only_after,
        follow_at=NEW.follow_at,
        displayname=NEW.displayname,
        sortorder=NEW.sortorder,
        trigger_type_id      =NEW.trigger_type_id,
        map_id               = NEW.map_id,
        poi_id               = NEW.poi_id,
        proximity_id         = NEW.proximity_id
    WHERE id = NEW.id
RETURNING *,'' as map_uuid;
CREATE OR REPLACE RULE audiotour_ins AS
    ON
INSERT TO
api.audiotour
DO
INSTEAD
INSERT INTO private.audiotour
    (track_id,
    transition_type_id,
    transition_options,
    media_id,
    does_loop,
    does_duck,
    on_weather,
    on_tod,
    fires_on_count,
    fires_on_count_global,
    sortorder,
    trigger_type_id,
    map_id,
    poi_id,
    only_once,
    session_skip,
    never_after,
    only_after,
    follow_at,
    displayname,
    proximity_id)
VALUES
    (NEW.track_id,
        NEW.transition_type_id,
        NEW.transition_options,
        NEW.media_id,
        NEW.does_loop,
        NEW.does_duck,
        NEW.on_weather,
        NEW.on_tod,
        NEW.fires_on_count,
        NEW.fires_on_count_global,
        NEW.sortorder,
        NEW.trigger_type_id,
        NEW.map_id,
        NEW.poi_id,
        NEW.only_once,
        NEW.session_skip,
        NEW.never_after,
        NEW.only_after,
        NEW.follow_at,
        NEW.displayname,
        NEW.proximity_id)
RETURNING *,'' as map_uuid;
CREATE OR REPLACE RULE audiotour_del AS ON
DELETE TO api.audiotour DO
INSTEAD
DELETE
    FROM private.audiotour
    WHERE id = OLD.id
RETURNING *,'' as map_uuid;
GRANT SELECT ON api.audiotour TO webanon;
GRANT SELECT, INSERT, DELETE, UPDATE ON api.audiotour TO webuser;