
DROP FUNCTION IF EXISTS drop_role_if_exists
(text);
CREATE OR REPLACE FUNCTION drop_role_if_exists
(_rolename text)
    returns void as
$$
DECLARE
    cmd text;
BEGIN
    IF EXISTS(SELECT 1
    FROM pg_roles
    WHERE rolname = _rolename) THEN
        cmd := 'DROP OWNED BY ' || _rolename;
EXECUTE cmd;
cmd := 'DROP ROLE ' || _rolename;
EXECUTE cmd;
END
IF;
END
$$ LANGUAGE plpgsql;


CREATE EXTENSION
IF NOT EXISTS pgcrypto;
DO
$$
BEGIN
    CREATE TYPE private.jwt_token AS
    (
            token text
        );
EXCEPTION
        WHEN duplicate_object THEN null;
END
$$;

-- Extract claims
DROP FUNCTION IF EXISTS private.extractClaims
(token text);
CREATE OR REPLACE FUNCTION private.extractClaims
(token text) RETURNS json
    LANGUAGE PLPGSQL AS
$$
DECLARE
BEGIN
    RETURN (
        SELECT payload::jsonb
    FROM public.verify(
                extractClaims.token,
                '~JWT_SECRET~',
                'HS256'));
END;
$$;


-- // Security ////////////////////////////////////////
DROP FUNCTION IF EXISTS private.check_role_exists
(text, text) CASCADE;
CREATE OR REPLACE FUNCTION private.check_role_exists
(_email text, _group_role text) RETURNS VOID
    LANGUAGE PLPGSQL AS
$$
DECLARE
    cmd text;
BEGIN
    SET ROLE websuper;
    -- because CREATEROLE isn't inherited
    IF NOT exists(select 1
    from pg_roles as r
    where r.rolname = _email) then
        cmd := 'CREATE ROLE "' || _email || '"';
    EXECUTE cmd;
END
IF;
    cmd := 'REVOKE webuser FROM "' || _email || '"';
EXECUTE cmd;
cmd := 'REVOKE websuper FROM "' || _email || '"';
EXECUTE cmd;
cmd := 'REVOKE webanon FROM "' || _email || '"';
EXECUTE cmd;
cmd := 'GRANT "' || _group_role || '" TO "' || _email || '"';
EXECUTE cmd;
RESET ROLE;
end
$$;
DROP FUNCTION IF EXISTS private.check_role_exists_trigger
() CASCADE;
CREATE OR REPLACE FUNCTION private.check_role_exists_trigger
() RETURNS TRIGGER
    LANGUAGE PLPGSQL AS
$$
BEGIN
    PERFORM private.check_role_exists
(NEW.email, NEW.group_role);
return NEW;
end
$$;


CREATE OR REPLACE FUNCTION private.remove_role
(_rolname text) RETURNS VOID
    LANGUAGE PLPGSQL AS
$$
DECLARE
    cmd text;
BEGIN
    SET ROLE websuper;
    -- because CREATEROLE isn't inherited
    if exists(select 1
    from pg_roles as r
    where r.rolname = _rolname) then
        cmd := 'DROP ROLE "' || _rolname || '"';
    EXECUTE cmd;
end
if;
    RESET ROLE;
end
$$;
CREATE OR REPLACE FUNCTION private.remove_role_trigger
() RETURNS TRIGGER
    LANGUAGE PLPGSQL AS
$$
BEGIN
    SET ROLE websuper;
    PERFORM private.remove_role
    (NEW.email);
RESET ROLE;
return new;
end
$$;



DROP TRIGGER IF EXISTS ensure_user_role_exists
ON api.users;
CREATE CONSTRAINT TRIGGER ensure_user_role_exists
    AFTER
INSERT
        OR
UPDATE
    ON api.users
    FOR EACH ROW
EXECUTE PROCEDURE private
.check_role_exists_trigger
();

DROP TRIGGER IF EXISTS remove_user_role_on_delete
ON api.users;
CREATE TRIGGER remove_user_role_on_delete
    AFTER
DELETE
    ON api.users
FOR EACH ROW
EXECUTE PROCEDURE private
.remove_role_trigger
();

CREATE OR REPLACE FUNCTION private.encrypt_pass
() RETURNS TRIGGER
    LANGUAGE PLPGSQL AS
$$
BEGIN
    if tg_op = 'INSERT' or new.pass <> old.pass then
        new.pass = crypt
    (new.pass, gen_salt
    ('bf'));
end
if;
    return new;
end
$$;
DROP TRIGGER IF EXISTS encrypt_pass
ON api.users;
CREATE TRIGGER encrypt_pass
    BEFORE
INSERT
        OR
UPDATE
    ON api.users
    FOR EACH ROW
EXECUTE PROCEDURE private
.encrypt_pass
();


CREATE OR REPLACE FUNCTION private.user_id
(email text) RETURNS name
    LANGUAGE PLPGSQL AS
$$
begin
    return (
        select id
    from api.users
    where users.email = user_id.email
    );
end;
$$;

DROP FUNCTION IF EXISTS api.user_role_from_email
(text);
DROP FUNCTION IF EXISTS api.user_role
(text);
DROP FUNCTION IF EXISTS api.checkpass
(text, text);
CREATE OR REPLACE FUNCTION api.checkpass
(email text, pass text) RETURNS name
    LANGUAGE PLPGSQL AS
$$
declare
    _group_role name;
begin
    SET ROLE doadmin;
    select group_role
    from api.users
    where users.email = checkpass.email
        AND users.pass = crypt(checkpass.pass, users.pass)
    INTO _group_role;
RESET ROLE;
RETURN _group_role;
end;
$$;


-- login should be on your exposed schema
DROP FUNCTION IF EXISTS api.login
(text, text);
CREATE OR REPLACE FUNCTION api.login
(email text, pass text) RETURNS private.jwt_token AS
$$
declare
    _passcheck name;
    _id        name;
    result     private.jwt_token;
begin
    -- check email and password
    select api.checkpass(login.email, login.pass)
    into _passcheck;
    if _passcheck IS NULL THEN
        RAISE invalid_password using message = 'Invalid username or password';
end
if;

    select public.sign(
                   row_to_json(r), '~JWT_SECRET~'
               ) as token
from (
             SELECT group_role                                   AS role,
                    group_role                                   AS group_role,
                    login.email                                  as email,
                    extract(epoch from now())::integer + 60 * 60 as exp,
        name,
        picture,
        is_validated,
        force_newpass,
        id
    FROM (
                      select *
        from api.users
        where api.users.email = login.email
                  ) u
         ) r
into result;
return result;
end;
$$ LANGUAGE PLPGSQL;


-- Reauthorize
DROP FUNCTION IF EXISTS api.reauthorize
(token text);
CREATE OR REPLACE FUNCTION api.reauthorize
(token text) RETURNS private.jwt_token
    LANGUAGE PLPGSQL AS
$$
DECLARE
    _role  name;
    result private.jwt_token;
BEGIN
    SELECT public.sign(
                   row_to_json(r), '~JWT_SECRET~'
               ) AS token
    FROM (
             SELECT group_role                                   AS role,
                    group_role                                   AS group_role,
                    email                                        as email,
                    extract(epoch from now())::integer + 60 * 60 as exp,
            name,
            picture,
            is_validated,
            force_newpass,
            id
        FROM (
                      select *
            from api.users
            where api.users.email = (SELECT private.extractClaims(token) ->> 'email')
                  ) u
         ) r
    INTO result;
return result;
END;
$$;


DROP FUNCTION IF EXISTS api.check_heirarchy_for (_check_for text) CASCADE;

-- // Permissions /////////////////


DO
$do$
BEGIN
    IF NOT EXISTS (
      SELECT
    FROM pg_catalog.pg_roles
    -- SELECT list can be empty for this
    WHERE  rolname = 'webanon') THEN
    CREATE ROLE webanon
    nologin;
END
IF;
END
$do$;


DO
$do$
BEGIN
    IF NOT EXISTS (
      SELECT
    FROM pg_catalog.pg_roles
    -- SELECT list can be empty for this
    WHERE  rolname = 'webuser') THEN
    CREATE ROLE webuser
    nologin;
END
IF;
END
$do$;

DO
$do$
BEGIN
    IF NOT EXISTS (
      SELECT
    FROM pg_catalog.pg_roles
    -- SELECT list can be empty for this
    WHERE  rolname = 'authenticator') THEN
    CREATE ROLE authenticator
    noinherit;
END
IF;
END
$do$;

DO
$do$
BEGIN
    IF NOT EXISTS (
      SELECT
    FROM pg_catalog.pg_roles
    -- SELECT list can be empty for this
    WHERE  rolname = 'websuper') THEN
    CREATE ROLE websuper
    CREATEROLE nologin;
END
IF;
END
$do$;


GRANT webanon to webuser
WITH ADMIN OPTION;
GRANT webanon to websuper
WITH ADMIN OPTION;
GRANT webuser to websuper
WITH ADMIN OPTION;


GRANT USAGE ON SCHEMA api TO webanon;
GRANT USAGE ON SCHEMA api,private TO webanon;
GRANT EXECUTE ON FUNCTION api.login
(text,text) TO webanon;
GRANT EXECUTE ON FUNCTION private.remove_role
(text) TO websuper;
GRANT EXECUTE ON FUNCTION private.extractClaims
(text) TO webuser;
GRANT EXECUTE ON FUNCTION api.checkpass
(text,text) TO webanon;
GRANT EXECUTE ON FUNCTION private.check_role_exists
(text,text) TO webuser;
GRANT EXECUTE ON FUNCTION api.check_heirarchy_for
(text) TO webanon;
GRANT EXECUTE ON FUNCTION api.reauthorize
(text) TO webuser;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA PRIVATE TO webuser;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA API TO webuser;
GRANT webanon TO authenticator;

-- Rebuild existing user roles
SELECT private.remove_role(roles.rolname)
FROM (select rolname
    from pg_roles
    where rolname not in (select email
        from api.users)
        AND rolname NOT LIKE 'pg%'
        AND rolname NOT IN ('webanon', 'webuser', 'websuper', 'authenticator', 'doadmin', 'postgres', '_dodb')
     ) AS roles;
SELECT private.check_role_exists(userlist.email, userlist.group_role)
FROM (SELECT *
    from api.users) AS userlist;

GRANT SELECT ON api.users TO webanon;
GRANT UPDATE ON api.users TO webuser;
GRANT INSERT, DELETE ON api.users TO websuper;


-- DROP RLS
ALTER TABLE api.users
    DISABLE ROW LEVEL SECURITY;

DROP POLICY
IF EXISTS users_select_policy ON api.users;


DROP POLICY
IF EXISTS users_delete_policy ON api.users;

DROP POLICY
IF EXISTS users_insert_policy ON api.users;

DROP POLICY
IF EXISTS users_update_policy ON api.users;