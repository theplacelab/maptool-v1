window._env_ = {
  REACT_APP_BASE_URL: 'https://steps.toasterlab.com',
  REACT_APP_ASSET_SERVER: 'https://assets.toasterlab.com',
  REACT_APP_GOOGLE_API_KEY: 'AIzaSyC-EAFTApfocAteXot07RW-4lwQzvt5u1s',
  REACT_APP_MAPBOX_API_KEY: '',
  REACT_APP_MODE: 'API',
  REACT_APP_PROJECT_LOGO: '',
  REACT_APP_PROJECT_PUBLISHER_NAME: 'Toasterlab',
  REACT_APP_SERVER_S3_APPID: '8edaca6c-98dc-4106-9d7e-a2b2119c9ea6',
  REACT_APP_SENTRY_API:
    'https://a3701c5730144e5c8f58c46f67ae502e@sentry.io/1407251',
  REACT_APP_BUG_REPORT_URL: 'https://gitlab.com/theplacelab/issues/issues/new',
  REACT_APP_HELP_URL:
    'https://docs.theplacelab.com/maptool/docs/user/introduction/',
  REACT_APP_OPENWEATHER_API_KEY: 'f7f3c2cc9525dd3701a7b2a5c1de575f',
  REACT_APP_HEAP_ID: '2769334934'
};
