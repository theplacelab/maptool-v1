---
name: Welcome
route: /
---

# Maptool

** [Version: 1.0.0](https://semver.org/) **  
This codebase is formatted with [Prettier](https://prettier.io/).  
&copy; 2020 **[Place Lab Ltd.](https://theplacelab.com)**  
[MIT Licensed](license)

> If you have a running instance of Maptool and want help with how to use it, you want the [📗 User Guide](userguide).

The Maptool consists of two major components: client and server, running on top of a PostgREST API, all bundled together and served via Docker.

The system is architected so that you can add or remove additional services as needed. For instance the core client itself can run in standalone mode using JSON files for data. If you want it to support geosearch, you can provide a Google API Key. If you want to edit POI, you can then add the API. If you want to add media support, you can provide an S3 key pair for file storage and so on.

The '.env' file is the key to getting this all to work. There is a mechanism for switching between multiple environments. There is also a configuration generator script which will help keep things organized.

This respositiory contains:

- The _maptool client_, a react-based app built around mapboxGL which provides an interface for CRUD operations for points on a map and associated metadata. This code integrates with a number of microservices including:

- _The API_ which is defined as a series if SQL scripts. This is intended to be served via Postgres and PostgREST.

- The maptool _server_, a node application which assists with tasks like emailing passwords and signing S3 links for uploading.

- A collection of deployment and configuration scripts which make the whole thing easier to maintain and distribute.

### Directory Tour

This repository uses yarn workspaces. The client code is primarily located in /packages/client, and the server code is in /packages/server. The rest of the directories provide additional functionality. Check out the package.json script block for entry points to most things.

```
.
├── database
├── deploy
├── environments
├── packages
│   ├── client
│   └── server
├── userguide

```
